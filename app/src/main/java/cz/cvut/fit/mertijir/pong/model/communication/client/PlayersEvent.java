package cz.cvut.fit.mertijir.pong.model.communication.client;

import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;
import org.apache.commons.math3.fraction.Fraction;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiri Mertin on 1.2.2016.
 */
public class PlayersEvent implements AJServiceSupportedSerializable {
    private final transient LocalEventValues mEventValues;
    private final transient int mWidth;
    private final transient int mHeight;

    private Fraction mTouchXTop;
    private Fraction mTouchXBottom;
    private Fraction mTouchYLeft;
    private Fraction mTouchYRight;
    private Float mSensorMotionX;
    private Float mSensorMotionY;

    public PlayersEvent(@NonNull LocalEventValues eventValues, int width, int height) {
        mEventValues = eventValues;
        mWidth = width;
        mHeight = height;
    }

    public void renew() {
        mTouchXTop = new Fraction(mEventValues.getTouchXTop(), mWidth);
        mTouchXBottom = new Fraction(mEventValues.getTouchXBottom(), mWidth);
        mTouchYLeft = new Fraction(mEventValues.getTouchYLeft(), mHeight);
        mTouchYRight = new Fraction(mEventValues.getTouchYRight(), mHeight);
        mSensorMotionY = mEventValues.getSensorMotionY();
        mSensorMotionX = mEventValues.getSensorMotionX();
    }

    public Fraction getTouchXTop() {
        return mTouchXTop;
    }

    public Fraction getTouchXBottom() {
        return mTouchXBottom;
    }

    public Fraction getTouchYLeft() {
        return mTouchYLeft;
    }

    public Fraction getTouchYRight() {
        return mTouchYRight;
    }

    public Float getSensorMotionX() {
        return mSensorMotionX;
    }

    public Float getSensorMotionY() {
        return mSensorMotionY;
    }

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendPlayersEvent(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }
}
