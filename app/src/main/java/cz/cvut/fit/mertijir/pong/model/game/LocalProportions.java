package cz.cvut.fit.mertijir.pong.model.game;

import android.support.annotation.NonNull;

import org.apache.commons.math3.fraction.Fraction;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;
import cz.cvut.fit.mertijir.pong.model.game.bat.WallBat;

/**
 * Created by Jiří Mertin on 7.1.17.
 */

public class LocalProportions {
    private static final int SHIFT_SPEED_MINIMAL = 8; //2^8
    private static final int SHIFT_SPEED_MAXIMAL = 4; //2^4

    private static final int SHIFT_SIZE_BALL = 4; //2^4
    private static final int SHIFT_SPEED_BALL_START = 7; //2^7
    private static final int SHIFT_SPEED_BALL_INCREMENT = 8; //2^8

    private static final int SHIFT_SPEED_BAT_TOUCH = 5; //2^5
    private static final int SHIFT_SPEED_BAT_TILT_MULTIPLIER = 6; //2^6

    private static final int SHIFT_MAX_SPEED_BAT_COMPUTER_BASE = 4;
    private static final int SHIFT_MAS_SPEED_BAT_COMPUTER_DECREMENT = 8;

    private static final int DIVIDER_MAX_SPEED_BAT_EASY = 4;
    private static final int DIVIDER_MAX_SPEED_BAT_MEDIUM = 3;
    private static final int DIVIDER_MAX_SPEED_BAT_HARD = 2;

    private static final int DIVIDER_SIZE_WALL_BAT = 5;

    private final LocalRealSurfaceSize mLocalRealSurfaceSize;

    private int mWidth;
    private int mHeight;

    public LocalProportions(@NonNull LocalRealSurfaceSize surfaceSize) {
        mLocalRealSurfaceSize = surfaceSize;
        resetToLocal();
    }

    public int getWallBatSize(@NonNull Position position) {
        switch (position) {
            case LEFT:
            case RIGHT:
                return getHeight() / DIVIDER_SIZE_WALL_BAT;
            case TOP:
            case BOTTOM:
            default:
                return getWidth() / DIVIDER_SIZE_WALL_BAT;
        }
    }

    public int getBallSize() {
        return getShorterSize() >> SHIFT_SIZE_BALL;
    }

    public int getSpeedMinimal() {
        return getShorterSize() >> SHIFT_SPEED_MINIMAL;
    }

    public int getSpeedMaximal() {
        return getShorterSize() >> SHIFT_SPEED_MAXIMAL;
    }

    public int getBallSpeedStart() {
        return getShorterSize() >> SHIFT_SPEED_BALL_START;
    }

    public int getBallSpeedIncrement() {
        return getShorterSize() >> SHIFT_SPEED_BALL_INCREMENT;
    }

    public int getBatSpeedTouch() {
        return getShorterSize() >> SHIFT_SPEED_BAT_TOUCH;
    }

    public int getBatSpeedTiltMultiplier() {
        return getShorterSize() >> SHIFT_SPEED_BAT_TILT_MULTIPLIER;
    }

    public int getMaxBatSpeed(PongPreferences.BatControlType controlType) {
        if (controlType == null) return 0;
        switch (controlType) {
            case TILT:
                return Integer.MAX_VALUE;
            case TOUCH:
                return getBatSpeedTouch();
        }
        int computerSpeed = getShorterSize() >> SHIFT_MAX_SPEED_BAT_COMPUTER_BASE;
        switch (controlType) {
            case CPU_EASY:
                computerSpeed /= DIVIDER_MAX_SPEED_BAT_EASY;
                break;
            case CPU_MEDIUM:
                computerSpeed /= DIVIDER_MAX_SPEED_BAT_MEDIUM;
                break;
            case CPU_HARD:
                computerSpeed /= DIVIDER_MAX_SPEED_BAT_HARD;
                break;
        }
        computerSpeed -= getShorterSize() >> SHIFT_MAS_SPEED_BAT_COMPUTER_DECREMENT;
        return computerSpeed;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getShorterSize() {
        return getWidth();
    }

    public enum Position {
        LEFT, RIGHT, TOP, BOTTOM
    }

    public static class WallBatDecision {
        final List<WallBat> mLeftBats = new ArrayList<>();
        final List<WallBat> mRightBats = new ArrayList<>();
        final List<WallBat> mTopBats = new ArrayList<>();
        final List<WallBat> mBottomBats = new ArrayList<>();

        public WallBatDecision(List<WallBat> batList) {
            for (WallBat bat : batList) {
                switch (bat.getPosition()) {
                    case LEFT:
                        mLeftBats.add(bat);
                        break;
                    case RIGHT:
                        mRightBats.add(bat);
                        break;
                    case TOP:
                        mTopBats.add(bat);
                        break;
                    case BOTTOM:
                        mBottomBats.add(bat);
                        break;
                }
            }
        }

        public WallBat getNextBatFromLastPosition(Position last) {
            switch (last) {
                case LEFT:
                    return mRightBats.get(App.getRandom().nextInt(mRightBats.size()));
                case RIGHT:
                    return mLeftBats.get(App.getRandom().nextInt(mLeftBats.size()));
                case TOP:
                    return mBottomBats.get(App.getRandom().nextInt(mBottomBats.size()));
                case BOTTOM:
                    return mTopBats.get(App.getRandom().nextInt(mTopBats.size()));
            }
            return null;
        }

        public WallBat getFirstBat(LocalBall ball) {
            if (mLeftBats.size() != 0 && ball.getSpeedX() < 0) {
                return getNextBatFromLastPosition(Position.RIGHT);
            }
            if (mRightBats.size() != 0 && ball.getSpeedX() > 0) {
                return getNextBatFromLastPosition(Position.LEFT);
            }
            if (mTopBats.size() != 0 && ball.getSpeedY() < 0) {
                return getNextBatFromLastPosition(Position.BOTTOM);
            }
            if (mBottomBats.size() != 0 && ball.getSpeedY() > 0) {
                return getNextBatFromLastPosition(Position.TOP);
            }
            return null;
        }
    }

    public void changeSizesByBestProportion(@NonNull Fraction remoteProportion) {
        resetToLocal();
        if (remoteProportion.compareTo(Fraction.ONE) > 0) {
            remoteProportion = remoteProportion.reciprocal();
        }
        if (remoteProportion.compareTo(mLocalRealSurfaceSize.getProportion()) > 0) {
            mHeight = getShorterSize() * remoteProportion.getDenominator() / remoteProportion.getNumerator();
        }
    }

    public Fraction getSurfaceProportion() {
        return new Fraction(mWidth, mHeight);
    }

    public void resetToLocal() {
        mWidth = mLocalRealSurfaceSize.getWidth();
        mHeight = mLocalRealSurfaceSize.getHeight();
    }

    public LocalRealSurfaceSize getLocalRealSurfaceSize() {
        return mLocalRealSurfaceSize;
    }
}
