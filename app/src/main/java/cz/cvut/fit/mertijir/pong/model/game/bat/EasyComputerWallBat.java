package cz.cvut.fit.mertijir.pong.model.game.bat;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;

/**
 * Created by Jiří Mertin on 15.10.16.
 */
public class EasyComputerWallBat extends ComputerWallBat {
    public EasyComputerWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
    }

    @Override
    protected void endingMovement(LocalBall ball) {
        final int maximalSpeed = getMaximalSpeed();
        switch (getPosition()) {
            case LEFT:
            case RIGHT:
                int absSpeedY = ball.getAbsSpeedY();
                if (ball.getMiddleY() > mY + getSizeY() / 2) {
                    if (absSpeedY == 0) mY += maximalSpeed;
                    mY += (maximalSpeed > absSpeedY) ? absSpeedY : maximalSpeed;
                    if (ball.getMiddleY() < mY + getSizeY() / 2) {
                        mY = ball.getMiddleY() - getSizeY() / 2;
                    }
                    break;
                }
                if (ball.getMiddleY() < mY + getSizeY() / 2) {
                    if (absSpeedY == 0) mY -= maximalSpeed;
                    mY -= (maximalSpeed > absSpeedY) ? absSpeedY : maximalSpeed;
                    if (ball.getMiddleY() > mY + getSizeY() / 2) {
                        mY = ball.getMiddleY() - getSizeY() / 2;
                    }
                    break;
                }
                break;
            default:
                int absSpeedX = ball.getAbsSpeedX();
                if (ball.getMiddleX() > mX + getSizeX() / 2) {
                    if (absSpeedX == 0) mX += maximalSpeed;
                    mX += (maximalSpeed > absSpeedX) ? absSpeedX : maximalSpeed;
                    if (ball.getMiddleX() < mX + getSizeX() / 2) {
                        mX = ball.getMiddleX() - getSizeX() / 2;
                    }
                    break;
                }
                if (ball.getMiddleX() < mX + getSizeX() / 2) {
                    if (absSpeedX == 0) mX -= maximalSpeed;
                    mX -= (maximalSpeed > absSpeedX) ? absSpeedX : maximalSpeed;
                    if (ball.getMiddleX() > mX + getSizeX() / 2) {
                        mX = ball.getMiddleX() - getSizeX() / 2;
                    }
                    break;
                }
                break;
        }
    }

    @Override
    public PongPreferences.BatControlType getControlType() {
        return PongPreferences.BatControlType.CPU_EASY;
    }
}
