package cz.cvut.fit.mertijir.pong.view.game;

import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.R;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.controller.allJoyn.Communicator;
import cz.cvut.fit.mertijir.pong.controller.allJoyn.CommunicatorListener;
import cz.cvut.fit.mertijir.pong.model.communication.DataRequest;
import cz.cvut.fit.mertijir.pong.model.communication.DataType;
import cz.cvut.fit.mertijir.pong.model.communication.GameAction;
import cz.cvut.fit.mertijir.pong.model.communication.client.DefaultSurfaceProportion;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.GameStats;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.info.ClientCurrentGameInfo;
import cz.cvut.fit.mertijir.pong.model.game.info.CurrentGameInfo;
import cz.cvut.fit.mertijir.pong.model.game.info.LocalCurrentGameInfo;

public class GameActivity extends AppCompatActivity implements LocalGameView.IPlayerWon {
    private GameView mGameView;
    private boolean mLeave = false;

    private boolean mServerPlayerPrepared;
    private boolean mJoinerPlayerPrepared;

    private CurrentGameInfo mCurrentGameInfo;
    private final GameStats mGameStats = new GameStats();

    @BindView(R.id.gameActivity_topStatistics)
    LinearLayout mTopPlayerStats;
    @BindView(R.id.gameActivity_bottomStatistics)
    LinearLayout mBottomPlayerStats;

    private AlertDialog mAlertDialog;
    private boolean mGameHasEnded = false;
    private Communicator.ConnectionState mConnectionState;
    private GameActivityCommunicationListener mGameActivityCommunicationListener;

    @BindView(R.id.gameActivity_youAreBottomPlayer_button)
    Button mButtonURBottom;
    @BindView(R.id.gameActivity_youAreTopPlayer_button)
    Button mButtonURTop;

    @OnClick({R.id.gameActivity_youAreBottomPlayer_button, R.id.gameActivity_youAreTopPlayer_button})
    void clickIAmThisPlayer() {
        mButtonURTop.setVisibility(View.INVISIBLE);
        mButtonURBottom.setVisibility(View.INVISIBLE);
        switch (mConnectionState) {
            case JOINED:
                App.getAJCommunicator().sendGameAction(new GameAction(GameAction.ActionType.PLAYER_PREPARED));
                break;
            case HOSTED:
                mServerPlayerPrepared = true;
                runLoop();
                break;
        }
    }

    private void runLoop() {
        switch (mConnectionState) {
            case HOSTED:
                if (mServerPlayerPrepared && mJoinerPlayerPrepared) {
                    mGameView.setRunningLoop(true);
                }
                break;
            default:
                mGameView.setRunningLoop(true);
        }
    }

    private void stopLoop() {
        mGameView.setRunningLoop(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mConnectionState = App.getAJCommunicator().getState();
        initiatePlayerInfo();
        setContentView(R.layout.game_activity_relative_layout);
        ButterKnife.bind(this);

        switch (mConnectionState) {
            case NONE:
                mGameView = new LocalGameView(this);
                runLoop();
                break;
            case HOSTED:
                mGameView = new ServerGameView(this);
                stopLoop();
                mButtonURBottom.setVisibility(View.VISIBLE);
                mButtonURBottom.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mButtonURBottom.performClick();
                    }
                }, 3000);
                break;
            case JOINED:
                mGameView = new ClientGameView(this, App.getInstance().getClientSurfacePreparation());
                runLoop();
                mButtonURTop.setVisibility(View.VISIBLE);
                mButtonURTop.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mButtonURTop.performClick();
                    }
                }, 3000);
                break;
        }

        FrameLayout gameViewParent = (FrameLayout) findViewById(R.id.gameActivity_gameView_frameLayout);
        gameViewParent.addView(mGameView);

        //more full screen
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        FrameLayout.LayoutParams gvLayoutParams = (FrameLayout.LayoutParams) mGameView.getLayoutParams();
        gvLayoutParams.width = App.getLocalProportions().getWidth();
        gvLayoutParams.height = App.getLocalProportions().getHeight();
        gvLayoutParams.gravity = Gravity.CENTER;

        mGameView.setOnClickListener(null);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(mGameView, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);

        updatePlayerInGameStats();
        mGameActivityCommunicationListener = new GameActivityCommunicationListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.getAJCommunicator().addListener(mGameActivityCommunicationListener);
        switch (mConnectionState) {
            case HOSTED:
                playerWon();
                break;
            case JOINED:
                App.getAJCommunicator().sendDataRequest(new DataRequest(DataType.GAME_STATS));
                break;
        }
    }

    private void initiatePlayerInfo() {
        if (mConnectionState == null) return;
        PongPreferences.GamePlayType gamePlayType = PongPreferences.getGamePlayType();
        if (mConnectionState == Communicator.ConnectionState.JOINED) {
            mCurrentGameInfo = new ClientCurrentGameInfo(App.getInstance().getClientSurfacePreparation());
            return;
        }
        mCurrentGameInfo = new LocalCurrentGameInfo(gamePlayType);
    }

    @Override
    public void onBackPressed() {
        createPauseDialog();
    }

    private void resumeGame() {
        runLoop();
        mAlertDialog = null;
        if (mConnectionState != Communicator.ConnectionState.NONE) {
            App.getAJCommunicator().sendGameAction(new GameAction(GameAction.ActionType.START));
        }
    }

    private void createPauseDialog() {
        if (mAlertDialog != null) return;
        stopLoop();
        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
        builder.setTitle(getString(R.string.gameActivity_dialogPause_title));
        builder.setMessage(getString(R.string.gameActivity_dialogPause_message));
        builder.setPositiveButton(R.string.gameActivity_dialogs_optionLeave,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        leaveGameAndSendStop();
                    }
                });
        builder.setNegativeButton(R.string.gameActivity_dialogPause_optionContinue,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resumeGame();
                    }
                });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                resumeGame();
            }
        });
        mAlertDialog = builder.create();
        if (mConnectionState != Communicator.ConnectionState.NONE) {
            App.getAJCommunicator().sendGameAction(new GameAction(GameAction.ActionType.PAUSE));
        }
        mAlertDialog.show();
    }

    private void leaveGame() {
        mLeave = true;
        GameActivity.super.onBackPressed();
    }

    private void createConnectionLostDialog() {
        mTopPlayerStats.post(new Runnable() {
            @Override
            public void run() {
                stopLoop();
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(getString(R.string.gameActivity_dialogConnectionLost_title));
                builder.setMessage(getString(R.string.gameActivity_dialogConnectionLost_message));
                builder.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                leaveGame();
                            }
                        });
                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        leaveGame();
                    }
                });
                mAlertDialog = builder.create();
                mAlertDialog.show();
            }
        });
    }

    @Override
    protected void onPause() {
        if (!mLeave) createPauseDialog();
        super.onPause();
        App.getAJCommunicator().removeListener(mGameActivityCommunicationListener);
    }

    @Override
    public void playerWon(LocalProportions.Position... positions) {
        if (!(mCurrentGameInfo instanceof LocalCurrentGameInfo)) return;
        LocalCurrentGameInfo currentGameInfo = (LocalCurrentGameInfo) mCurrentGameInfo;
        for (LocalProportions.Position position : positions) {
            switch (position) {
                case TOP:
                    currentGameInfo.playerJustScored(CurrentGameInfo.PlayerPosition.TOP);
                    break;
                case BOTTOM:
                    currentGameInfo.playerJustScored(CurrentGameInfo.PlayerPosition.BOTTOM);
                    break;
            }
        }
        updatePlayerInGameStats();
        if (mConnectionState == null
                || mConnectionState == Communicator.ConnectionState.NONE) return;
        currentGameInfo.updateGameStats(mGameStats);
        App.getAJCommunicator().sendGameStats(mGameStats);
    }

    @Override
    public boolean gameHasEnded() {
        return mGameHasEnded;
    }

    private void updatePlayerInGameStats() {
        updatePlayerInGameStats(mTopPlayerStats, mCurrentGameInfo.getPlayerInfo(CurrentGameInfo.PlayerPosition.TOP));
        updatePlayerInGameStats(mBottomPlayerStats, mCurrentGameInfo.getPlayerInfo(CurrentGameInfo.PlayerPosition.BOTTOM));
        LocalCurrentGameInfo.PlayerPosition currentGameWinner = mCurrentGameInfo.getWinnerOfCurrentGame();
        if (currentGameWinner == null) return;
        App.getAJCommunicator().sendGameAction(new GameAction(
                (currentGameWinner == CurrentGameInfo.PlayerPosition.TOP)
                        ? GameAction.ActionType.WINNER_UPPER
                        : GameAction.ActionType.WINNER_BOTTOM));
        createGameEndDialog(currentGameWinner);
    }

    private void leaveGameAndSendStop() {
        leaveGame();
        if (mConnectionState != Communicator.ConnectionState.NONE) {
            App.getAJCommunicator().sendGameAction(new GameAction(GameAction.ActionType.STOP));
        }
    }

    private void createGameEndDialog(CurrentGameInfo.PlayerPosition currentGameWinner) {
        mGameHasEnded = true;
        stopLoop();
        final AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
        builder.setTitle(getString(R.string.gameActivity_dialogGameFinished_title));
        builder.setMessage(getString(currentGameWinner == CurrentGameInfo.PlayerPosition.TOP ? R.string.gameActivity_dialogGameFinished_messageWinnerTop : R.string.gameActivity_dialogGameFinished_messageWinnerBottom));
        builder.setPositiveButton(R.string.gameActivity_dialogs_optionLeave,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        leaveGameAndSendStop();
                    }
                });

        builder.setNegativeButton(R.string.gameActivity_dialogGameFinished_optionRepeat,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        initiatePlayerInfo();
                        updatePlayerInGameStats();
                        playerWon();
                        mGameHasEnded = false;
                        resumeGame();
                    }
                });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                leaveGameAndSendStop();
            }
        });

        mBottomPlayerStats.post(new Runnable() {
            @Override
            public void run() {
                mAlertDialog = builder.create();
                // display dialog
                mAlertDialog.show();
            }
        });
    }

    private void updatePlayerInGameStats(LinearLayout playerStats, final LocalCurrentGameInfo.PlayerInfo playerInfo) {
        if (playerStats == null || playerInfo == null) return;
        final TextView lives = ButterKnife.findById(playerStats, R.id.lives);
        final TextView points = ButterKnife.findById(playerStats, R.id.points);
        if (lives == null || points == null) return;

        if (mCurrentGameInfo.isLivesShown()) {
            String tempLives = "";
            for (int i = 0; i < playerInfo.getCountOfLives(); i++) {
                tempLives = tempLives + getString(R.string.gameActivity_oneLife);
            }
            final String textLives = tempLives;
            lives.post(new Runnable() {
                @Override
                public void run() {
                    lives.setText(textLives);
                    lives.setVisibility(View.VISIBLE);
                }
            });
        } else {
            lives.post(new Runnable() {
                @Override
                public void run() {
                    lives.setVisibility(View.INVISIBLE);
                }
            });
        }
        points.post(new Runnable() {
            @Override
            public void run() {
                points.setText(String.format(Locale.getDefault(), "%d", playerInfo.getCountOfPoints()));
                points.setVisibility(mCurrentGameInfo.isPointsShown() ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    private class GameActivityCommunicationListener implements CommunicatorListener {

        @Override
        public void localHostCreated(boolean created, @Nullable String name) {
        }

        @Override
        public void remoteHostAvailability(boolean available) {
        }

        @Override
        public void localJoinedToRemoteHost(boolean connected, @Nullable String name) {
            if (mConnectionState == Communicator.ConnectionState.JOINED && !connected) {
                createConnectionLostDialog();
            }
        }

        @Override
        public void remoteJoinedToLocalHost(boolean has, @Nullable String joinerName) {
            if (mConnectionState == Communicator.ConnectionState.HOSTED && !has) {
                createConnectionLostDialog();
            }
        }

        @Override
        public void receivedDefaultSurfaceProportions(DefaultSurfaceProportion remoteProportions) {
        }

        @Override
        public void receivedPlayersEvents(PlayersEvent playersEvent) {
            if (mConnectionState != Communicator.ConnectionState.HOSTED) return;
            if (mGameView instanceof ServerGameView) {
                ((ServerGameView) mGameView).setRemotePlayersEvent(playersEvent);
            }
        }

        @Override
        public void receivedObjectPositions(ObjectsPosition objectsPosition) {
            if (mConnectionState != Communicator.ConnectionState.JOINED) return;
            if (mGameView instanceof ClientGameView) {
                ((ClientGameView) mGameView).setRemoteObjectsPosition(objectsPosition);
            }
        }

        @Override
        public void receivedSurfacePreparation(SurfacePreparation surfacePreparation) {
        }

        @Override
        public void cannotJoinWhenHosting() {
        }

        @Override
        public void cannotHostWhenJoined() {
        }

        @Override
        public void receivedDataRequest(DataRequest dataRequest) {
            if (mConnectionState == Communicator.ConnectionState.HOSTED) {
                if (dataRequest.mDataType == DataType.GAME_STATS) {
                    playerWon();
                }
            }
        }

        @Override
        public void receivedGameAction(final GameAction gameAction) {
            if (mConnectionState == Communicator.ConnectionState.NONE) return;
            mGameView.post(new Runnable() {
                @Override
                public void run() {
                    switch (gameAction.mActionType) {
                        case PLAYER_PREPARED:
                            mJoinerPlayerPrepared = true;
                            runLoop();
                            break;
                        case PAUSE:
                            createPauseDialog();
                            break;
                        case STOP:
                            if (mAlertDialog != null) {
                                mAlertDialog.dismiss();
                                mAlertDialog = null;
                            }
                            mLeave = true;
                            GameActivity.super.onBackPressed();
                            break;
                        case START:
                            if (mConnectionState == Communicator.ConnectionState.HOSTED) {
                                initiatePlayerInfo();
                                updatePlayerInGameStats();
                                playerWon();
                            }
                            mGameHasEnded = false;
                            runLoop();
                            if (mAlertDialog != null) {
                                mAlertDialog.dismiss();
                                mAlertDialog = null;
                            }
                            break;
                        case WINNER_UPPER:
                            createGameEndDialog(CurrentGameInfo.PlayerPosition.TOP);
                            break;
                        case WINNER_BOTTOM:
                            createGameEndDialog(CurrentGameInfo.PlayerPosition.BOTTOM);
                            break;
                    }
                }
            });
        }

        @Override
        public void receivedGameStats(GameStats gameStats) {
            if (mConnectionState != Communicator.ConnectionState.JOINED) return;
            ClientCurrentGameInfo.ClientPlayerInfo playerInfo = (ClientCurrentGameInfo.ClientPlayerInfo)
                    mCurrentGameInfo.getPlayerInfo(CurrentGameInfo.PlayerPosition.TOP);
            playerInfo.setCountOfLives(gameStats.mTopCountOfLives);
            playerInfo.setCountOfPoints(gameStats.mTopCountOfPoints);
            playerInfo = (ClientCurrentGameInfo.ClientPlayerInfo)
                    mCurrentGameInfo.getPlayerInfo(CurrentGameInfo.PlayerPosition.BOTTOM);
            playerInfo.setCountOfLives(gameStats.mBottomCountOfLives);
            playerInfo.setCountOfPoints(gameStats.mBottomCountOfPoints);
            mGameView.post(new Runnable() {
                @Override
                public void run() {
                    updatePlayerInGameStats(mTopPlayerStats, mCurrentGameInfo.getPlayerInfo(CurrentGameInfo.PlayerPosition.TOP));
                    updatePlayerInGameStats(mBottomPlayerStats, mCurrentGameInfo.getPlayerInfo(CurrentGameInfo.PlayerPosition.BOTTOM));
                }
            });
        }
    }
}
