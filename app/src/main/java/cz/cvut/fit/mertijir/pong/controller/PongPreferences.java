package cz.cvut.fit.mertijir.pong.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;

import cz.cvut.fit.mertijir.pong.R;

/**
 * Created by Jiří Mertin on 1.1.17.
 */

public class PongPreferences extends Preference {
    private static PongPreferences sInstance;

    @Override
    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    private PongPreferences(Context context) {
        super(context);
    }

    public static void initPreferences(Context context) {
        sInstance = new PongPreferences(context);
    }

    public static BatControlType getGameControlBottom1Type() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_batControl_bottom1_key);
        return getControlTypeByKeyValue(sInstance.getSharedPreferences().getString(key, null));
    }

    public static BatControlType getGameControlBottom2Type() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_batControl_bottom2_key);
        return getControlTypeByKeyValue(sInstance.getSharedPreferences().getString(key, null));
    }

    public static BatControlType getGameControlUpper1Type() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_batControl_upper1_key);
        return getControlTypeByKeyValue(sInstance.getSharedPreferences().getString(key, null));
    }

    public static BatControlType getGameControlUpper2Type() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_batControl_upper2_key);
        return getControlTypeByKeyValue(sInstance.getSharedPreferences().getString(key, null));
    }

    public static String getGameControlTextFromType(@NonNull BatControlType type) {
        switch (type) {
            case TOUCH:
                return sInstance.getContext().getString(R.string.gamePreferences_batControl_touch_text);
            case TILT:
                return sInstance.getContext().getString(R.string.gamePreferences_batControl_tilt_text);
            case CPU_EASY:
                return sInstance.getContext().getString(R.string.gamePreferences_batControl_easyCPU_text);
            case CPU_MEDIUM:
                return sInstance.getContext().getString(R.string.gamePreferences_batControl_mediumCPU_text);
            case CPU_HARD:
                return sInstance.getContext().getString(R.string.gamePreferences_batControl_hardCPU_text);
        }
        return null;
    }

    public static boolean isUpperSecondaryBat() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_secondaryBat_upper_key);
        return sInstance.getSharedPreferences().getBoolean(key, false);
    }

    public static boolean isBottomSecondaryBat() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_secondaryBat_bottom_key);
        return sInstance.getSharedPreferences().getBoolean(key, false);
    }

    public static boolean isSecondaryBall() {
        String key = sInstance.getContext().getString(R.string.gamePreferences_ballSecondary_key);
        return sInstance.getSharedPreferences().getBoolean(key, false);
    }

    @ColorInt
    private static int getBatColorByNumber(int number) {
        int[] colors = sInstance.getContext().getResources().getIntArray(R.array.batColors);
        number %= colors.length;
        return colors[number];
    }

    @ColorInt
    public static int getBatColor(BatPosition batPosition) {
        return getBatColorByNumber(batPosition.ordinal());
    }

    public enum BatPosition {
        UPPER1, BOTTOM1, UPPER2, BOTTOM2
    }

    public enum BatControlType {
        TOUCH, TILT, CPU_EASY, CPU_MEDIUM, CPU_HARD
    }

    private static BatControlType getControlTypeByKeyValue(String keyValue) {
        if (keyValue == null || keyValue.equals(sInstance.getContext().getString(R.string.gamePreferences_batControl_touch_key))) {
            return BatControlType.TOUCH;
        }
        if (keyValue.equals(sInstance.getContext().getString(R.string.gamePreferences_batControl_tilt_key))) {
            return BatControlType.TILT;
        }
        if (keyValue.equals(sInstance.getContext().getString(R.string.gamePreferences_batControl_easyCPU_key))) {
            return BatControlType.CPU_EASY;
        }
        if (keyValue.equals(sInstance.getContext().getString(R.string.gamePreferences_batControl_mediumCPU_key))) {
            return BatControlType.CPU_MEDIUM;
        }
        if (keyValue.equals(sInstance.getContext().getString(R.string.gamePreferences_batControl_hardCPU_key))) {
            return BatControlType.CPU_HARD;
        }
        return BatControlType.TOUCH;
    }

    public enum GamePlayType {
        DEATH_MATCH, LIFE_DRAGGING, POINTS, INFINITY;

        public boolean isLivesShown() {
            switch (this) {
                case DEATH_MATCH:
                case LIFE_DRAGGING:
                    return true;
                case POINTS:
                case INFINITY:
                    return false;
            }
            return false;
        }

        public boolean isPointsShown() {
            switch (this) {
                case LIFE_DRAGGING:
                case POINTS:
                case INFINITY:
                    return true;
                case DEATH_MATCH:
                    return false;
            }
            return false;
        }
    }

    public static GamePlayType getGamePlayType() {
        String key = sInstance.getSharedPreferences().getString(sInstance.getContext().getString(R.string.gamePreferences_gameStyle_key), null);
        if (key == null || key.equals(sInstance.getContext().getString(R.string.gamePreferences_gameStyle_lifeDragging_key))) {
            return GamePlayType.LIFE_DRAGGING;
        }
        if (key.equals(sInstance.getContext().getString(R.string.gamePreferences_gameStyle_deathMatch_key))) {
            return GamePlayType.DEATH_MATCH;
        }
        if (key.equals(sInstance.getContext().getString(R.string.gamePreferences_gameStyle_points_key))) {
            return GamePlayType.POINTS;
        }
        if (key.equals(sInstance.getContext().getString(R.string.gamePreferences_gameStyle_infinitive_key))) {
            return GamePlayType.INFINITY;
        }
        return GamePlayType.LIFE_DRAGGING;
    }


    public static String getGamePlayTextFromType(@NonNull GamePlayType type) {
        switch (type) {
            case DEATH_MATCH:
                return sInstance.getContext().getString(R.string.gamePreferences_gameStyle_deathMatch_text);
            case LIFE_DRAGGING:
                return sInstance.getContext().getString(R.string.gamePreferences_gameStyle_lifeDragging_text);
            case POINTS:
                return sInstance.getContext().getString(R.string.gamePreferences_gameStyle_points_text);
            case INFINITY:
                return sInstance.getContext().getString(R.string.gamePreferences_gameStyle_infinitive_text);
        }
        return null;
    }
}
