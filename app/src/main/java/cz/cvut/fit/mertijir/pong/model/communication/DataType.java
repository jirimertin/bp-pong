package cz.cvut.fit.mertijir.pong.model.communication;

/**
 * Created by Jiří Mertin on 23.1.17.
 */
public enum DataType {
    DEFAULT_SURFACE_PROPORTION,
    PLAYERS_EVENT,
    OBJECT_POSITION,
    SURFACE_PREPARATION,
    GAME_STATS
}
