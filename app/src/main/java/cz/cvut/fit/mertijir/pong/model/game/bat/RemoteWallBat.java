package cz.cvut.fit.mertijir.pong.model.game.bat;

import org.apache.commons.math3.fraction.Fraction;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;

/**
 * Created by Jiří Mertin on 2.1.17.
 */

public class RemoteWallBat extends WallBat {
    public RemoteWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
    }

    @Override
    public void moveRemote(PlayersEvent playersEvent) {
    }

    @Override
    public PongPreferences.BatControlType getControlType() {
        return null;
    }

    public void setPositionByProportion(Fraction proportion) {
        switch (getPosition()) {
            case TOP:
            case BOTTOM:
                mX = App.getLocalProportions().getWidth() * proportion.getNumerator() / proportion.getDenominator();
                break;
            case LEFT:
            case RIGHT:
                mY = App.getLocalProportions().getHeight() * proportion.getNumerator() / proportion.getDenominator();
                break;
        }
    }
}
