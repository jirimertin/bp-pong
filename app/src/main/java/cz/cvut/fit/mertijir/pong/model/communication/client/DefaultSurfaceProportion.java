package cz.cvut.fit.mertijir.pong.model.communication.client;

import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;
import org.apache.commons.math3.fraction.Fraction;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.model.game.LocalRealSurfaceSize;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiří Mertin on 9.1.17.
 */

public class DefaultSurfaceProportion implements AJServiceSupportedSerializable {
    public Fraction mSurfaceProportion;

    public DefaultSurfaceProportion() {
    }

    public DefaultSurfaceProportion(LocalRealSurfaceSize surfaceSize) {
        if (surfaceSize != null) {
            mSurfaceProportion = surfaceSize.getProportion();
        }
    }

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendDefaultSurfaceProportion(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }
}
