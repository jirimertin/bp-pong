package cz.cvut.fit.mertijir.pong.model.game.bat;

import android.support.annotation.CallSuper;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;

/**
 * Created by Jiri Mertin on 23.12.2015.
 */
public abstract class WallBat extends Bat {
    protected int mX;
    protected int mY;
    protected int mSizeX;
    protected int mSizeY;
    protected final LocalProportions.Position POSITION;
    private final PongPreferences.BatPosition mBatPosition;

    public WallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(PongPreferences.getBatColor(batPosition));
        mBatPosition = batPosition;
        POSITION = position;
        if (position == null) throw new NullPointerException("Not defined position in the wall.");
        switch (position) {
            case LEFT:
            case RIGHT:
                mSizeX = App.getLocalProportions().getBallSize();
                mSizeY = App.getLocalProportions().getWallBatSize(position);
                mY = (App.getLocalProportions().getHeight() - mSizeY) >> 1;
                break;
            default:
                mSizeY = App.getLocalProportions().getBallSize();
                mSizeX = App.getLocalProportions().getWallBatSize(position);
                mX = (App.getLocalProportions().getWidth() - mSizeX) >> 1;
        }
        switch (position) {
            case LEFT:
                mX = 0;
                break;
            case TOP:
                mY = 0;
                break;
            case RIGHT:
                mX = App.getLocalProportions().getWidth() - getSizeX();
                break;
            case BOTTOM:
                mY = App.getLocalProportions().getHeight() - getSizeY();
        }
    }

    @Override
    public int getX() {
        return mX;
    }

    @Override
    public int getY() {
        return mY;
    }

    @Override
    public int getSizeX() {
        return mSizeX;
    }

    @Override
    public int getSizeY() {
        return mSizeY;
    }

    public LocalProportions.Position getPosition() {
        return POSITION;
    }

    @CallSuper
    @Override
    public void move(LocalEventValues eventValues) {
        //check bounds
        switch (POSITION) {
            case TOP:
            case BOTTOM:
                if (mX < 0) mX = 0;
                if (mX > App.getLocalProportions().getWidth() - getSizeX()) {
                    mX = App.getLocalProportions().getWidth() - getSizeX();
                }
                break;
            case LEFT:
            case RIGHT:
                if (mY < 0) mY = 0;
                if (mY > App.getLocalProportions().getHeight() - getSizeY()) {
                    mY = App.getLocalProportions().getHeight() - getSizeY();
                }
        }
    }

    public static WallBat generateBat(LocalProportions.Position position, PongPreferences.BatControlType controlType, PongPreferences.BatPosition batPosition) {
        if (position == null || controlType == null) return null;
        switch (controlType) {
            case TOUCH:
                return new TouchWallBat(position, batPosition);
            case TILT:
                return new TiltWallBat(position, batPosition);
            case CPU_EASY:
                return new EasyComputerWallBat(position, batPosition);
            case CPU_MEDIUM:
                return new MediumComputerWallBat(position, batPosition);
            case CPU_HARD:
                return new HardComputerWallBat(position, batPosition);
        }
        return null;
    }

    public PongPreferences.BatPosition getBatPosition() {
        return mBatPosition;
    }

    protected int getMaximalSpeed() {
        return App.getLocalProportions().getMaxBatSpeed(getControlType());
    }
}
