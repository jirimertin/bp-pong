package cz.cvut.fit.mertijir.pong.controller.allJoyn;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.model.communication.DataRequest;
import cz.cvut.fit.mertijir.pong.model.communication.GameAction;
import cz.cvut.fit.mertijir.pong.model.communication.client.DefaultSurfaceProportion;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.GameStats;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;

/**
 * Created by Jiří Mertin on 15.1.17.
 */
public class Communicator implements ServiceConnection, AllJoynService.ServiceListener {
    protected static final String TAG = Communicator.class.getName();
    private AllJoynService mService;

    public Communicator(Context context) {
        Intent bindIntent = new Intent(context, AllJoynService.class);
        context.bindService(bindIntent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d(TAG, "onServiceConnected");
        mService = ((AllJoynService.Binder) iBinder).getAllJoynService();
        mService.removeListener(this);
        mService.addListener(this);
        sendListenerData();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        Log.d(TAG, "onServiceDisconnected");
        mService = null;
    }

    protected AllJoynService getService() {
        return mService;
    }

    private final ArrayList<CommunicatorListener> mListeners = new ArrayList<>();

    private String mHostedChannelName;
    private HostedChannel.HostChannelState mHostChannelState = HostedChannel.HostChannelState.IDLE;
    private HostedChannel.Joiner mJoinerToHost;

    private String mJoinedChannelName;
    private UsedChannel.UseChannelState mJoinedChannelState;

    private final ArrayList<String> mFoundChannels = new ArrayList<>();

    private ConnectionState mState = ConnectionState.NONE;


    public void quit() {
        Log.i(TAG, "mHandler.handleMessage(): APPLICATION_QUIT_EVENT");
        leaveChannel();
        stopChannel();
        Log.i(TAG, "mBackgroundHandler.exit()");
        if (getService() == null) return;
        getService().stopSelf();
        getService().stopForeground(true);
    }

    public ArrayList<String> getFoundChannels() {
        return mFoundChannels;
    }

    private synchronized HostedChannel.HostChannelState getHostChannelState() {
        return mHostChannelState;
    }

    private synchronized String getHostedChannelName() {
        return mHostedChannelName;
    }

    private synchronized UsedChannel.UseChannelState getJoinedChannelState() {
        return mJoinedChannelState;
    }

    private synchronized String getJoinedChannelName() {
        return mJoinedChannelName;
    }

    public synchronized void joinChannel(String name) {
        if (mState == ConnectionState.HOSTED) {
            for (CommunicatorListener listener : mListeners) {
                listener.cannotJoinWhenHosting();
            }
            return;
        }
        leaveChannel();
        Log.i(TAG, "mHandler.handleMessage(): USE_JOIN_CHANNEL_EVENT");
        if (getService() == null) return;
        getService().callJoinSession(name);
    }

    public synchronized void leaveChannel() {
        logChannelState();//navic
        Log.i(TAG, "mHandler.handleMessage(): USE_LEAVE_CHANNEL_EVENT");
        if (mJoinedChannelName == null) return;
        if (getService() == null) return;
        getService().callLeaveSession();
    }

    public synchronized void startChannel(String name) {
        if (mState == ConnectionState.JOINED) {
            for (CommunicatorListener listener : mListeners) {
                listener.cannotHostWhenJoined();
            }
            return;
        }
        stopChannel();
        mHostedChannelName = name;
        logChannelState();
        Log.i(TAG, "mHandler.handleMessage(): HOST_START_CHANNEL_EVENT");
        if (getService() == null) return;
        getService().callRequestName(name);
        getService().callBindSession();
        getService().callAdvertise();
    }

    public synchronized void stopChannel() {
        logChannelState();
        Log.i(TAG, "mHandler.handleMessage(): HOST_STOP_CHANNEL_EVENT");
        if (mHostedChannelName == null) return;
        if (getService() != null) {
            getService().callCancelAdvertise();
            getService().callUnbindSession();
            getService().callReleaseName();
        }
        mHostedChannelName = null;
    }


    private void logChannelState() {
        Log.d(TAG, "Connection state == " + mState.name());
        switch (mState) {
            case HOSTED:
                Log.d(TAG, "Hosted Session Name >" + getHostedChannelName() + "< " + getHostChannelState().name());
                break;
            case JOINED:
                Log.d(TAG, "Joined Session Name >" + getJoinedChannelName() + "< " + getJoinedChannelState().name());
                break;
        }
    }

    public void addListener(CommunicatorListener listener) {
        mListeners.add(listener);
    }

    public void removeListener(CommunicatorListener listener) {
        mListeners.remove(listener);
    }

    @Override
    public void handleLocalHostUpdate(HostedChannel hostedChannel) {
        mHostChannelState = hostedChannel.getState();
        if (hostedChannel.getState() == HostedChannel.HostChannelState.ADVERTISED) {
            mHostedChannelName = hostedChannel.getName();
            mState = ConnectionState.HOSTED;
        } else {
            mState = ConnectionState.NONE;
        }
        logChannelState();
        sendCreatedHost();
    }

    @Override
    public void handleLocalJoinToRemoteHostUpdate(UsedChannel usedChannel) {
        mJoinedChannelState = usedChannel.getState();
        if (usedChannel.getState() == UsedChannel.UseChannelState.JOINED) {
            mJoinedChannelName = usedChannel.getName();
            mState = ConnectionState.JOINED;
        } else {
            mState = ConnectionState.NONE;
        }
        logChannelState();
        sendConnected();
    }

    @Override
    public void handleFoundRemoteHostUpdate(FoundChannel foundChannel) {
        if (foundChannel.getState() == FoundChannel.FoundChannelState.LOST) {
            mFoundChannels.remove(foundChannel.getName());
        } else {
            mFoundChannels.add(foundChannel.getName());
        }
        sendAvailableHosts();
    }

    @Override
    public void handleRemoteJoinToLocalHostUpdate(HostedChannel.Joiner joinedToHost) {
        getService().setUnableToJoinToHost(joinedToHost.isActive());
        mJoinerToHost = joinedToHost;
        sendJoinerToHost();
    }

    private void sendListenerData() {
        for (CommunicatorListener listener : mListeners) {
            sendListenerDataTo(listener);
        }
    }

    public void sendListenerDataTo(CommunicatorListener listener) {
        sendCreatedHostTo(listener);
        sendAvailableHostsTo(listener);
        sendConnectedTo(listener);
        sendJoinerToHostTo(listener);
    }

    private void sendAvailableHosts() {
        for (CommunicatorListener listener : mListeners) {
            sendAvailableHostsTo(listener);
        }
    }

    public void sendAvailableHostsTo(CommunicatorListener listener) {
        listener.remoteHostAvailability(!getFoundChannels().isEmpty());
    }

    private void sendCreatedHost() {
        for (CommunicatorListener listener : mListeners) {
            sendCreatedHostTo(listener);
        }
    }

    public void sendCreatedHostTo(CommunicatorListener listener) {
        listener.localHostCreated(HostedChannel.HostChannelState.ADVERTISED == getHostChannelState(), getHostedChannelName());
    }

    private void sendConnected() {
        for (CommunicatorListener listener : mListeners) {
            sendConnectedTo(listener);
        }
    }

    public void sendConnectedTo(CommunicatorListener listener) {
        listener.localJoinedToRemoteHost(UsedChannel.UseChannelState.JOINED == getJoinedChannelState(), getJoinedChannelName());
    }

    private void sendJoinerToHost() {
        for (CommunicatorListener listener : mListeners) {
            sendJoinerToHostTo(listener);
        }
    }

    public void sendJoinerToHostTo(CommunicatorListener listener) {
        if (mJoinerToHost == null) return;
        listener.remoteJoinedToLocalHost(mJoinerToHost.isActive(), mJoinerToHost.getName());
    }

    public void sendSurfacePreparation(SurfacePreparation surfacePreparation) {
        getService().callSendObject(surfacePreparation);
    }

    @Override
    public void handleReceivedObject(AJServiceSupportedSerializable serializable) {
        for (CommunicatorListener listener : mListeners) {
            if (serializable instanceof DefaultSurfaceProportion) {
                listener.receivedDefaultSurfaceProportions((DefaultSurfaceProportion) serializable);
            }
            if (serializable instanceof PlayersEvent) {
                listener.receivedPlayersEvents((PlayersEvent) serializable);
            }
            if (serializable instanceof ObjectsPosition) {
                listener.receivedObjectPositions((ObjectsPosition) serializable);
            }
            if (serializable instanceof SurfacePreparation) {
                listener.receivedSurfacePreparation((SurfacePreparation) serializable);
            }
            if (serializable instanceof DataRequest) {
                listener.receivedDataRequest((DataRequest) serializable);
            }
            if (serializable instanceof GameAction) {
                listener.receivedGameAction((GameAction) serializable);
            }
            if (serializable instanceof GameStats) {
                listener.receivedGameStats((GameStats) serializable);
            }
        }
    }

    public void sendDefaultSurfaceProportion(DefaultSurfaceProportion proportion) {
        getService().callSendObject(proportion);
    }

    public void sendGameAction(GameAction gameAction) {
        getService().callSendObject(gameAction);
    }

    public void sendPlayersEvent(PlayersEvent playersEvent) {
        getService().callSendObject(playersEvent);
    }

    public void sendObjectsPosition(ObjectsPosition objectsPosition) {
        getService().callSendObject(objectsPosition);
    }

    public void sendGameStats(GameStats gameStats) {
        getService().callSendObject(gameStats);
    }

    public void sendDataRequest(DataRequest dataRequest) {
        getService().callSendObject(dataRequest);
    }

    public enum ConnectionState {
        NONE, HOSTED, JOINED
    }

    public ConnectionState getState() {
        return mState;
    }

    /**
     * Created by Jiří Mertin on 15.1.17.
     */
    public abstract static class BaseChannel<State> {
        protected final String mNamePrefix;
        protected final String mName;
        protected final String mWellKnownName;
        private State mState;

        protected BaseChannel(String namePrefix, String name) {
            mNamePrefix = namePrefix;
            mName = name;
            mWellKnownName = namePrefix + '.' + name;
        }

        public String getNamePrefix() {
            return mNamePrefix;
        }

        public String getName() {
            return mName;
        }

        public String getWellKnownName() {
            return mWellKnownName;
        }

        public State getState() {
            return mState;
        }

        public void setState(State state) {
            mState = state;
        }
    }

    /**
     * Created by Jiří Mertin on 15.1.17.
     */
    public static class FoundChannel extends BaseChannel<FoundChannel.FoundChannelState> {
        private final short mPort;
        private boolean mJoined;

        public FoundChannel(String namePrefix, String nameWithPrefix, short port) {
            super(namePrefix, nameWithPrefix.replaceFirst(namePrefix + '.', ""));
            mPort = port;
            setState(FoundChannelState.ACTIVE);
        }

        public short getPort() {
            return mPort;
        }

        public boolean isJoined() {
            return mJoined;
        }

        public void setJoined(boolean joined) {
            mJoined = joined;
        }

        public enum FoundChannelState {
            ACTIVE,
            LOST
        }
    }

    /**
     * Created by Jiří Mertin on 15.1.17.
     */
    public static class HostedChannel extends BaseChannel<HostedChannel.HostChannelState> {
        /**
         * The state of the AllJoyn components responsible for hosting an chat channel.
         */
        private ArrayList<Joiner> mJoiners;
        private short mSessionPort;
        private boolean mJoinedToSelf;

        public HostedChannel(String namePrefix, String name) {
            super(namePrefix, name);
            setState(HostChannelState.IDLE);
        }

        public void setSessionPort(short sessionPort) {
            mSessionPort = sessionPort;
        }

        public short getSessionPort() {
            return mSessionPort;
        }

        public boolean isJoinedToSelf() {
            return mJoinedToSelf;
        }

        public void setJoinedToSelf(boolean joinedToSelf) {
            mJoinedToSelf = joinedToSelf;
        }

        public void addJoiner(Joiner joiner) {
            if (joiner == null) return;
            if (mJoiners == null) {
                mJoiners = new ArrayList<>(1);
            }
            mJoiners.add(joiner);
        }

        public void removeJoiner(Joiner joiner) {
            mJoiners.remove(joiner);
        }

        public void dropJoiners() {
            if (mJoiners == null) return;
            for (int i = mJoiners.size() - 1; i > -1; i--) {
                mJoiners.remove(i);
            }
        }

        public ArrayList<Joiner> getJoiners() {
            return (ArrayList<Joiner>) mJoiners.clone();
        }

        public boolean getJoinedToSelf() {
            return mJoinedToSelf;
        }

        /**
         * Enumeration of the states of a hosted chat channel.  This lets us make a
         * note to ourselves regarding where we are in the process of preparing
         * and tearing down the AllJoyn pieces responsible for providing the chat
         * service.  In order to be out of the IDLE state, the BusAttachment state
         * must be at least CONNECTED.
         */
        public enum HostChannelState {
            /**
             * There is no hosted chat channel
             */
            IDLE,
            /**
             * The well-known name for the channel has been successfully acquired
             */
            NAMED,
            /**
             * A session port has been bound for the channel
             */
            BOUND,
            /**
             * The bus attachment has advertised itself as hosting an chat channel
             */
            ADVERTISED
        }

        public static class Joiner {
            private final short mSessionPort;
            private final int mId;
            private final String mName;
            private boolean mActive;
            private final IPongCommunication mPongCommunicator;

            public Joiner(short sessionPort, int id, String name, IPongCommunication pongCommunication) {
                mSessionPort = sessionPort;
                mId = id;
                mName = name;
                mPongCommunicator = pongCommunication;
            }

            public short getSessionPort() {
                return mSessionPort;
            }

            public int getId() {
                return mId;
            }

            public String getName() {
                return mName;
            }

            public void setActive(boolean active) {
                mActive = active;
            }

            public boolean isActive() {
                return mActive;
            }

            public IPongCommunication getPongCommunicator() {
                return mPongCommunicator;
            }
        }
    }

    /**
     * Created by Jiří Mertin on 15.1.17.
     */
    public static class UsedChannel extends BaseChannel<UsedChannel.UseChannelState> {
        private int mSessionId;
        private IPongCommunication mPongCommunicator;

        public UsedChannel(String namePrefix, String name) {
            super(namePrefix, name);
            setState(UseChannelState.IDLE);
        }

        public UsedChannel(HostedChannel channel) {
            super(channel.getNamePrefix(), channel.getName());
            setState(UseChannelState.JOINED);
        }

        public void setSessionId(int sessionId) {
            mSessionId = sessionId;
        }

        public int getSessionId() {
            return mSessionId;
        }

        public void setPongCommunicator(IPongCommunication pongCommunicator) {
            mPongCommunicator = pongCommunicator;
        }

        public IPongCommunication getPongCommunicator() {
            return mPongCommunicator;
        }

        /**
         * Enumeration of the states of a hosted chat channel.  This lets us make a
         * note to ourselves regarding where we are in the process of preparing
         * and tearing down the AllJoyn pieces responsible for providing the chat
         * service.  In order to be out of the IDLE state, the BusAttachment state
         * must be at least CONNECTED.
         */
        public enum UseChannelState {
            /**
             * There is no used chat channel
             */
            IDLE,
            /**
             * The session for the channel has been successfully joined
             */
            JOINED
        }
    }
}
