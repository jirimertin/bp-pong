package cz.cvut.fit.mertijir.pong.model.game.bat;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;

/**
 * Created by Jiri Mertin on 23.12.2015.
 */
public class TouchWallBat extends WallBat {
    private int mTargetX;
    private int mTargetY;
    private final int sSpeed;

    public TouchWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
        mTargetX = getX();
        mTargetY = getY();
        sSpeed = App.getLocalProportions().getBatSpeedTouch();
    }

    @Override
    public void move(LocalEventValues eventValues) {
        if (eventValues == null) return;
        switch (getPosition()) {
            case LEFT:
                mTargetY = eventValues.getTouchYLeft() - getSizeY() / 2;
                break;
            case RIGHT:
                mTargetY = eventValues.getTouchYRight() - getSizeY() / 2;
                break;
            case TOP:
                mTargetX = eventValues.getTouchXTop() - getSizeX() / 2;
                break;
            case BOTTOM:
                mTargetX = eventValues.getTouchXBottom() - getSizeX() / 2;
        }
        continueToTarget();
        super.move(eventValues);
    }

    private void continueToTarget() {
        if (mTargetX != mX) {
            if (mTargetX > mX) {
                mX += sSpeed;
                if (mX > mTargetX) mX = mTargetX;
            }
            if (mTargetX < mX) {
                mX -= sSpeed;
                if (mX < mTargetX) mX = mTargetX;
            }
        }
        if (mTargetY != mY) {
            if (mTargetY > mY) {
                mY += sSpeed;
                if (mY > mTargetY) mY = mTargetY;
            }
            if (mTargetY < mY) {
                mY -= sSpeed;
                if (mY < mTargetY) mY = mTargetY;
            }
        }
    }

    @Override
    public void moveRemote(PlayersEvent playersEvent) {
        if (playersEvent == null) return;
        switch (getPosition()) {
            case LEFT:
                mTargetY = App.getLocalProportions().getHeight()
                        * playersEvent.getTouchYLeft().getNumerator() / playersEvent.getTouchYLeft().getDenominator()
                        - getSizeY() / 2;
                break;
            case RIGHT:
                mTargetY = App.getLocalProportions().getHeight()
                        * playersEvent.getTouchYRight().getNumerator() / playersEvent.getTouchYRight().getDenominator()
                        - getSizeY() / 2;
                break;
            case TOP:
                mTargetX = App.getLocalProportions().getWidth()
                        * playersEvent.getTouchXTop().getNumerator() / playersEvent.getTouchXTop().getDenominator()
                        - getSizeX() / 2;
                break;
            case BOTTOM:
                mTargetX = App.getLocalProportions().getWidth()
                        * playersEvent.getTouchXBottom().getNumerator() / playersEvent.getTouchXBottom().getDenominator()
                        - getSizeX() / 2;
        }
        continueToTarget();
        super.move(null);
    }


    @Override
    public PongPreferences.BatControlType getControlType() {
        return PongPreferences.BatControlType.TOUCH;
    }
}
