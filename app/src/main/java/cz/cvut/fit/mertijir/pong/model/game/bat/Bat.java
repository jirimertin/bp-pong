package cz.cvut.fit.mertijir.pong.model.game.bat;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.ColorInt;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;

/**
 * Created by Jiri Mertin on 17.12.2015.
 */
public abstract class Bat {
    @ColorInt
    private final int mColor;

    public Bat(@ColorInt int color) {
        mColor = color;
    }

    public Rect getDrawableRect() {
        return new Rect(getX(), getY(), getX() + getSizeX(), getY() + getSizeY());
    }

    public abstract int getX();

    public abstract int getY();

    public abstract int getSizeX();

    public abstract int getSizeY();

    public abstract void move(LocalEventValues eventValues);

    public abstract void moveRemote(PlayersEvent playersEvent);

    public abstract PongPreferences.BatControlType getControlType();

    public void draw(Canvas canvas) {
        Paint color = new Paint();
        color.setColor(this.getColor());
        canvas.drawRect(getDrawableRect(), color);
    }

    @ColorInt
    public int getColor() {
        return mColor;//PongPreferences.getBatColorFromType(getControlType());
    }
}
