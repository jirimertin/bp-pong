package cz.cvut.fit.mertijir.pong;

import android.app.Application;

import java.util.Random;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.controller.allJoyn.Communicator;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;

/**
 * Created by Jiří Mertin on 17.9.16.
 */
public class App extends Application {
    private static App sInstance;
    private final Random mRandom = new Random(System.currentTimeMillis());
    private LocalProportions mLocalProportions;
    private SurfacePreparation mClientSurfacePreparation;

    private Communicator mCommunicator;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        PongPreferences.initPreferences(this);
        mCommunicator = new Communicator(this);
    }

    public static App getInstance() {
        return sInstance;
    }

    public static Random getRandom() {
        return getInstance().mRandom;
    }

    public static Communicator getAJCommunicator() {
        return getInstance().mCommunicator;
    }

    public static LocalProportions getLocalProportions() {
        return getInstance().mLocalProportions;
    }

    public static void setLocalProportions(LocalProportions localProportions) {
        getInstance().mLocalProportions = localProportions;
    }

    public SurfacePreparation getClientSurfacePreparation() {
        return mClientSurfacePreparation;
    }

    public void setClientSurfacePreparation(SurfacePreparation clientSurfacePreparation) {
        mClientSurfacePreparation = clientSurfacePreparation;
    }
}
