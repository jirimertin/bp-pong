package cz.cvut.fit.mertijir.pong.model.game;

import org.apache.commons.math3.fraction.Fraction;

import java.io.Serializable;

/**
 * Created by Jiri Mertin on 1.2.2016.
 */
public class LocalRealSurfaceSize implements Serializable {
    private final int mWidth;
    private final int mHeight;

    private final Fraction mProportion;

    public LocalRealSurfaceSize(int x, int y) {
        if (x > y) {
            int temp = x;
            x = y;
            y = temp;
        }
        mWidth = x;
        mHeight = y;
        mProportion = new Fraction(x, y);
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public Fraction getProportion() {
        return mProportion;
    }
}
