package cz.cvut.fit.mertijir.pong.model.communication.server;

import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiří Mertin on 29.1.17.
 */

public class GameStats implements AJServiceSupportedSerializable {
    public int mTopCountOfLives;
    public int mTopCountOfPoints;
    public int mBottomCountOfLives;
    public int mBottomCountOfPoints;

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendGameStats(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }
}
