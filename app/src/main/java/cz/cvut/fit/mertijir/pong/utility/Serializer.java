package cz.cvut.fit.mertijir.pong.utility;

import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by Jiří Mertin on 19.1.17.
 * http://stackoverflow.com/questions/8887197/reliably-convert-any-object-to-string-and-then-back-again
 */

public class Serializer {
    public static String serialize(Serializable serializable) {
        String s = null;
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(serializable);
            so.flush();
            s = new String(Base64.encode(bo.toByteArray(), Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public static <SERIALIZABLE extends Serializable> SERIALIZABLE deserialize(String string64, Class<SERIALIZABLE> aClass) {
        SERIALIZABLE serializable = null;
        try {
            byte b[] = Base64.decode(string64.getBytes(), Base64.DEFAULT);
            ByteArrayInputStream bi = new ByteArrayInputStream(b);
            ObjectInputStream si = new ObjectInputStream(bi);
            Object object = si.readObject();
            serializable = (SERIALIZABLE) object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serializable;
    }
}
