package cz.cvut.fit.mertijir.pong.model.game.bat;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;

/**
 * Created by Jiri Mertin on 23.12.2015.
 */
public class TiltWallBat extends WallBat {
    public TiltWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
    }

    @Override
    public void move(LocalEventValues eventValues) {
        if (eventValues == null) return;
        switch (getPosition()) {
            case LEFT:
            case RIGHT:
                mY += eventValues.getSensorMotionY() * App.getLocalProportions().getBatSpeedTiltMultiplier();
                break;
            default:
                mX += eventValues.getSensorMotionX() * App.getLocalProportions().getBatSpeedTiltMultiplier();
        }
        super.move(eventValues);
    }

    @Override
    public void moveRemote(PlayersEvent playersEvent) {
        if (playersEvent == null) return;
        switch (getPosition()) {
            case LEFT:
            case RIGHT:
                mY += playersEvent.getSensorMotionY() * App.getLocalProportions().getBatSpeedTiltMultiplier();
                break;
            case TOP:
            case BOTTOM:
                mX += playersEvent.getSensorMotionX() * App.getLocalProportions().getBatSpeedTiltMultiplier();
                break;
        }
        super.move(null);
    }

    @Override
    public PongPreferences.BatControlType getControlType() {
        return PongPreferences.BatControlType.TILT;
    }
}
