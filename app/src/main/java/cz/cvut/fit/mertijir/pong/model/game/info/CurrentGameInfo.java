package cz.cvut.fit.mertijir.pong.model.game.info;

import android.support.annotation.NonNull;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.communication.server.GameStats;

/**
 * Created by Jiří Mertin on 29.1.17.
 */
public abstract class CurrentGameInfo {
    protected static final int LIFE_NUMBER = 3;
    protected static final int MAX_POINT_NUMBER = 10;
    protected final PongPreferences.GamePlayType mStyle;
    private final boolean mLivesShown;
    private final boolean mPointsShown;

    public CurrentGameInfo(@NonNull PongPreferences.GamePlayType styleOfPlay) {
        mStyle = styleOfPlay;
        mLivesShown = getStyle().isLivesShown();
        mPointsShown = getStyle().isPointsShown();
    }

    protected PongPreferences.GamePlayType getStyle() {
        return mStyle;
    }

    public abstract PlayerInfo getPlayerInfo(@NonNull PlayerPosition position);

    public abstract PlayerPosition getWinnerOfCurrentGame();

    public boolean isLivesShown() {
        return mLivesShown;
    }

    public boolean isPointsShown() {
        return mPointsShown;
    }

    public void updateGameStats(GameStats gameStats) {
        if (gameStats == null) return;
        gameStats.mTopCountOfPoints = getPlayerInfo(PlayerPosition.TOP).getCountOfPoints();
        gameStats.mTopCountOfLives = getPlayerInfo(PlayerPosition.TOP).getCountOfLives();
        gameStats.mBottomCountOfPoints = getPlayerInfo(PlayerPosition.BOTTOM).getCountOfPoints();
        gameStats.mBottomCountOfLives = getPlayerInfo(PlayerPosition.BOTTOM).getCountOfLives();
    }

    public enum PlayerPosition {
        TOP,
        BOTTOM
    }

    /**
     * Created by Jiří Mertin on 1.9.16.
     */
    public abstract static class PlayerInfo {
        protected int mCountOfLives;
        protected int mCountOfPoints;

        public int getCountOfLives() {
            return mCountOfLives;
        }

        public int getCountOfPoints() {
            return mCountOfPoints;
        }
    }
}
