package cz.cvut.fit.mertijir.pong.model.communication.server;

import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;
import org.apache.commons.math3.fraction.Fraction;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiří Mertin on 9.1.17.
 */

public class SurfacePreparation implements AJServiceSupportedSerializable {
    public final Fraction mSurfaceProportion;

    public final boolean mUpperSecondaryBatIsActive;
    public final boolean mBottomSecondaryBatIsActive;
    public final boolean mSecondBallIsActive;

    public final PongPreferences.GamePlayType mStyleOfPlay;


    public SurfacePreparation(@NonNull LocalProportions localProportions, @NonNull PongPreferences.GamePlayType styleOfPlay) {
        mSurfaceProportion = localProportions.getSurfaceProportion();
        mUpperSecondaryBatIsActive = PongPreferences.isUpperSecondaryBat();
        mBottomSecondaryBatIsActive = PongPreferences.isBottomSecondaryBat();
        mSecondBallIsActive = PongPreferences.isSecondaryBall();
        mStyleOfPlay = styleOfPlay;
    }

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendSurfacePreparation(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }
}
