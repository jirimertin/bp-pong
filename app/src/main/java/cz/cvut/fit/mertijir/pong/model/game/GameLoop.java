package cz.cvut.fit.mertijir.pong.model.game;

import android.graphics.Canvas;

import cz.cvut.fit.mertijir.pong.view.game.GameView;

/**
 * Created by Jiri Mertin on 11.12.2015.
 */
public class GameLoop extends Thread {
    static final long FRAMES_PER_SECOND = 40;
    private final GameView mGameView;
    private boolean running = false;

    public GameLoop(GameView view) {
        mGameView = view;
    }

    public void setRunning(boolean run) {
        running = run;
    }

    @Override
    public void run() {
        long timePerScreen = 1000 / FRAMES_PER_SECOND;
        long startTime;
        long sleepTime;
        while (running) {
            Canvas c = null;
            startTime = System.currentTimeMillis();
            try {
                mGameView.update();
                c = mGameView.getHolder().lockCanvas();
                synchronized (mGameView.getHolder()) {
                    mGameView.draw(c);
                }
            } finally {
                if (c != null) {
                    mGameView.getHolder().unlockCanvasAndPost(c);
                }
            }
            sleepTime = timePerScreen - (System.currentTimeMillis() - startTime);
            try {
                if (sleepTime > 0) {
                    sleep(sleepTime);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
