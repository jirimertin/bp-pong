package cz.cvut.fit.mertijir.pong.model.communication.server;

import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;
import org.apache.commons.math3.fraction.Fraction;

import java.io.Serializable;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;
import cz.cvut.fit.mertijir.pong.model.game.bat.WallBat;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiří Mertin on 9.1.17.
 */

public class ObjectsPosition implements AJServiceSupportedSerializable {
    public static class BallStruct implements Serializable {
        public Fraction mX;
        public Fraction mY;
        @ColorInt
        public Integer mColor;
    }

    public BallStruct mBallPrimary;
    public BallStruct mBallSecondary;

    public Fraction mUpperPrimaryBat;
    public Fraction mUpperSecondaryBat;
    public Fraction mBottomPrimaryBat;
    public Fraction mBottomSecondaryBat;

    public void setBat(WallBat bat, int width) {
        Fraction fraction = new Fraction(bat.getX(), width);
        switch (bat.getBatPosition()) {
            case UPPER1:
                mUpperPrimaryBat = fraction;
                break;
            case UPPER2:
                mUpperSecondaryBat = fraction;
                break;
            case BOTTOM1:
                mBottomPrimaryBat = fraction;
                break;
            case BOTTOM2:
                mBottomSecondaryBat = fraction;
                break;
        }
    }

    public void setBall(LocalBall localBall, int width, int height) {
        BallStruct ballStruct = new BallStruct();
        ballStruct.mX = new Fraction(localBall.getX(), width);
        ballStruct.mY = new Fraction(localBall.getY(), height);
        ballStruct.mColor = localBall.getCollisionWallBat().getColor();
        if (localBall.isPrimaryBall()) {
            mBallPrimary = ballStruct;
        } else {
            mBallSecondary = ballStruct;
        }
    }

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendObjectsPosition(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }
}
