package cz.cvut.fit.mertijir.pong.view.game;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;
import cz.cvut.fit.mertijir.pong.model.game.ball.RemoteBall;
import cz.cvut.fit.mertijir.pong.model.game.bat.RemoteWallBat;

/**
 * Created by Jiří Mertin on 23.1.17.
 */
public class ClientGameView extends GameView {
    private final ArrayList<RemoteWallBat> mBats = new ArrayList<>();
    private final ArrayList<RemoteBall> mBalls = new ArrayList<>();
    private final SurfacePreparation mSurfacePreparation;
    private ObjectsPosition mRemoteObjectsPosition;
    private PlayersEvent mLocalPlayersEvent;

    public ClientGameView(GameActivity gameActivity, SurfacePreparation surfacePreparation) {
        super(gameActivity);
        mSurfacePreparation = surfacePreparation;
    }

    @Override
    protected void createGameObjects() {
        mBats.add(new RemoteWallBat(LocalProportions.Position.TOP, PongPreferences.BatPosition.UPPER1));
        mBats.add(new RemoteWallBat(LocalProportions.Position.BOTTOM, PongPreferences.BatPosition.BOTTOM1));
        if (mSurfacePreparation.mUpperSecondaryBatIsActive) {
            mBats.add(new RemoteWallBat(LocalProportions.Position.TOP, PongPreferences.BatPosition.UPPER2));
        }
        if (mSurfacePreparation.mBottomSecondaryBatIsActive) {
            mBats.add(new RemoteWallBat(LocalProportions.Position.BOTTOM, PongPreferences.BatPosition.BOTTOM2));
        }
        mBalls.add(new RemoteBall());
        if (mSurfacePreparation.mSecondBallIsActive) {
            mBalls.add(new RemoteBall());
        }
        mLocalPlayersEvent = new PlayersEvent(mEventValues, getWidth(), getHeight());
    }

    @Override
    protected ArrayList<RemoteBall> getBalls() {
        return mBalls;
    }

    public void setRemoteObjectsPosition(ObjectsPosition remoteObjectsPosition) {
        mRemoteObjectsPosition = remoteObjectsPosition;
    }

    @Override
    public void update() {
        mLocalPlayersEvent.renew();
        App.getAJCommunicator().sendPlayersEvent(mLocalPlayersEvent);
        if (mRemoteObjectsPosition == null) return;
        for (RemoteWallBat bat : mBats) {
            switch (bat.getBatPosition()) {
                case UPPER1:
                    bat.setPositionByProportion(mRemoteObjectsPosition.mUpperPrimaryBat);
                    break;
                case UPPER2:
                    bat.setPositionByProportion(mRemoteObjectsPosition.mUpperSecondaryBat);
                    break;
                case BOTTOM1:
                    bat.setPositionByProportion(mRemoteObjectsPosition.mBottomPrimaryBat);
                    break;
                case BOTTOM2:
                    bat.setPositionByProportion(mRemoteObjectsPosition.mBottomSecondaryBat);
                    break;
            }
        }
        for (int i = 0; i < mBalls.size(); i++) {
            switch (i) {
                case 0:
                    mBalls.get(i).setPositionProportions(mRemoteObjectsPosition.mBallPrimary.mX, mRemoteObjectsPosition.mBallPrimary.mY);
                    mBalls.get(i).setColor(mRemoteObjectsPosition.mBallPrimary.mColor);
                    break;
                case 1:
                    mBalls.get(i).setPositionProportions(mRemoteObjectsPosition.mBallSecondary.mX, mRemoteObjectsPosition.mBallSecondary.mY);
                    mBalls.get(i).setColor(mRemoteObjectsPosition.mBallSecondary.mColor);
                    break;
            }
        }
    }

    @Override
    public ArrayList<RemoteWallBat> getBats() {
        return mBats;
    }
}
