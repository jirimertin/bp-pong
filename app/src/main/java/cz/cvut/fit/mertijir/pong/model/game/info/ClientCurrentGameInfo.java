package cz.cvut.fit.mertijir.pong.model.game.info;

import android.support.annotation.NonNull;

import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;

/**
 * Created by Jiří Mertin on 29.1.17.
 */
public class ClientCurrentGameInfo extends CurrentGameInfo {
    private final ClientPlayerInfo mTopPlayerInfo = new ClientPlayerInfo();
    private final ClientPlayerInfo mBottomPlayerInfo = new ClientPlayerInfo();

    public ClientCurrentGameInfo(@NonNull SurfacePreparation surface) {
        super(surface.mStyleOfPlay);
    }

    @Override
    public ClientPlayerInfo getPlayerInfo(@NonNull PlayerPosition position) {
        switch (position) {
            case TOP:
                return mTopPlayerInfo;
            case BOTTOM:
                return mBottomPlayerInfo;
        }
        return null;
    }

    @Override
    public PlayerPosition getWinnerOfCurrentGame() {
        return null;
    }

    public static class ClientPlayerInfo extends PlayerInfo {
        public void setCountOfLives(int countOfLives) {
            mCountOfLives = countOfLives;
        }

        public void setCountOfPoints(int countOfPoints) {
            mCountOfPoints = countOfPoints;
        }
    }
}
