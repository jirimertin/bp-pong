package cz.cvut.fit.mertijir.pong.model.communication;

import android.support.annotation.NonNull;

import java.io.Serializable;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;

/**
 * Created by Jiří Mertin on 21.1.17.
 */

public interface AJServiceSupportedSerializable extends Serializable {
    void sendThrough(@NonNull IPongCommunication pongCommunicator);
}
