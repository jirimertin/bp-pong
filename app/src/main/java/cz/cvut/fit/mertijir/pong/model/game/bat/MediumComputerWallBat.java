package cz.cvut.fit.mertijir.pong.model.game.bat;

import java.util.Random;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;

/**
 * Created by Jiří Mertin on 15.10.16.
 */
public class MediumComputerWallBat extends ComputerWallBat {
    public MediumComputerWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
    }

    @Override
    protected void endingMovement(LocalBall ball) {
        final int maximalSpeed = getMaximalSpeed();
        Random random = App.getRandom();
        switch (getPosition()) {
            case LEFT:
            case RIGHT:
                mY += random.nextFloat() * ((random.nextBoolean()) ? maximalSpeed : -maximalSpeed);
                break;
            default:
                mX += random.nextFloat() * ((random.nextBoolean()) ? maximalSpeed : -maximalSpeed);
                break;
        }
    }

    @Override
    public PongPreferences.BatControlType getControlType() {
        return PongPreferences.BatControlType.CPU_MEDIUM;
    }
}
