package cz.cvut.fit.mertijir.pong.model.game;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.view.MotionEvent;
import android.view.Surface;

import java.io.Serializable;
import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;

/**
 * Created by Jiri Mertin on 19.12.2015.
 */
public class LocalEventValues implements Serializable {

    private int mTouchYLeft;
    private int mTouchYRight;
    private int mTouchXTop;
    private int mTouchXBottom;
    private float mSensorMotionX;
    private float mSensorMotionY;
    private ArrayList<LocalBall> mBalls;

    public LocalEventValues() {
        mTouchYLeft = App.getLocalProportions().getHeight() >> 1;
        mTouchYRight = mTouchYLeft;
        mTouchXTop = App.getLocalProportions().getWidth() >> 1;
        mTouchXBottom = mTouchXTop;
    }

    public void saveMotionEvent(MotionEvent motionEvent) {
        int xTop, xBottom, yLeft, yRight;
        int xTopCnt, xBottomCnt, yLeftCnt, yRightCnt;
        xTop = xBottom = yLeft = yRight = 0;
        xTopCnt = xBottomCnt = yLeftCnt = yRightCnt = 0;
        int pointerID;
        for (int i = 0; i < motionEvent.getPointerCount(); i++) {
            pointerID = motionEvent.getPointerId(i);
            try {
                if (motionEvent.getX(pointerID) > App.getLocalProportions().getWidth() * 2 / 5) {
                    yRight += motionEvent.getY(pointerID);
                    yRightCnt++;
                }
                if (motionEvent.getX(pointerID) < App.getLocalProportions().getWidth() * 3 / 5) {
                    yLeft += motionEvent.getY(pointerID);
                    yLeftCnt++;
                }
                if (motionEvent.getY(pointerID) > App.getLocalProportions().getHeight() * 2 / 5) {
                    xBottom += motionEvent.getX(pointerID);
                    xBottomCnt++;
                }
                if (motionEvent.getY(pointerID) < App.getLocalProportions().getHeight() * 3 / 5) {
                    xTop += motionEvent.getX(pointerID);
                    xTopCnt++;
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        if (yRightCnt > 0) {
            mTouchYRight = yRight / yRightCnt;
        }
        if (yLeftCnt > 0) {
            mTouchYLeft = yLeft / yLeftCnt;
        }
        if (xBottomCnt > 0) {
            mTouchXBottom = xBottom / xBottomCnt;
        }
        if (xTopCnt > 0) {
            mTouchXTop = xTop / xTopCnt;
        }
    }

    public void saveSensorEvent(Activity activity, SensorEvent sensorEvent) {
        switch (sensorEvent.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                switch (activity.getWindowManager().getDefaultDisplay().getRotation()) {
                    case Surface.ROTATION_0:
                        mSensorMotionX = -sensorEvent.values[0];
                        mSensorMotionY = sensorEvent.values[1];
                        break;
                    case Surface.ROTATION_90:
                        mSensorMotionX = sensorEvent.values[1];
                        mSensorMotionY = sensorEvent.values[0];
                        break;
                    case Surface.ROTATION_180:
                        mSensorMotionX = sensorEvent.values[0];
                        mSensorMotionY = -sensorEvent.values[1];
                        break;
                    case Surface.ROTATION_270:
                        mSensorMotionX = -sensorEvent.values[1];
                        mSensorMotionY = -sensorEvent.values[0];
                        break;
                }
        }
    }

    public int getTouchYLeft() {
        return mTouchYLeft;
    }

    public int getTouchYRight() {
        return mTouchYRight;
    }

    public int getTouchXTop() {
        return mTouchXTop;
    }

    public int getTouchXBottom() {
        return mTouchXBottom;
    }

    public float getSensorMotionX() {
        return mSensorMotionX;
    }

    public float getSensorMotionY() {
        return mSensorMotionY;
    }

    public void saveBalls(ArrayList<LocalBall> balls) {
        mBalls = balls;
    }

    public ArrayList<LocalBall> getBalls() {
        return mBalls;
    }
}
