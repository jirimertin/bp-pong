package cz.cvut.fit.mertijir.pong.controller.allJoyn;

import android.support.annotation.Nullable;

import cz.cvut.fit.mertijir.pong.model.communication.DataRequest;
import cz.cvut.fit.mertijir.pong.model.communication.GameAction;
import cz.cvut.fit.mertijir.pong.model.communication.client.DefaultSurfaceProportion;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.GameStats;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;

/**
 * Created by Jiří Mertin on 22.1.17.
 */
public interface CommunicatorListener {
    void localHostCreated(boolean created, @Nullable String name);

    void remoteHostAvailability(boolean available);

    void localJoinedToRemoteHost(boolean connected, @Nullable String name);

    void remoteJoinedToLocalHost(boolean has, @Nullable String joinerName);

    void receivedDefaultSurfaceProportions(DefaultSurfaceProportion remoteProportions);

    void receivedPlayersEvents(PlayersEvent playersEvent);

    void receivedObjectPositions(ObjectsPosition objectsPosition);

    void receivedSurfacePreparation(SurfacePreparation surfacePreparation);

    void cannotJoinWhenHosting();

    void cannotHostWhenJoined();

    void receivedDataRequest(DataRequest dataRequest);

    void receivedGameAction(GameAction gameAction);

    void receivedGameStats(GameStats gameStats);
}
