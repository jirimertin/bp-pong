package cz.cvut.fit.mertijir.pong.model.communication;

import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiří Mertin on 23.1.17.
 */

public class GameAction implements AJServiceSupportedSerializable {
    public final ActionType mActionType;

    public GameAction(ActionType actionType) {
        mActionType = actionType;
    }

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendGameAction(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }

    public enum ActionType {
        START,
        START_CONFIRMATION,
        PLAYER_PREPARED,
        PAUSE,
        STOP,
        WINNER_UPPER,
        WINNER_BOTTOM
    }
}
