package cz.cvut.fit.mertijir.pong.model.game.info;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;

/**
 * Created by Jiří Mertin on 14.10.16.
 */

public class LocalCurrentGameInfo extends CurrentGameInfo {
    private final LocalPlayerInfo mPlayerTop;
    private final LocalPlayerInfo mPlayerBottom;

    public LocalCurrentGameInfo(@NonNull PongPreferences.GamePlayType styleOfPlay) {
        super(styleOfPlay);
        mPlayerTop = new LocalPlayerInfo(getStyle());
        mPlayerBottom = new LocalPlayerInfo(getStyle());
        reset();
    }

    @Nullable
    public PlayerPosition getWinnerOfCurrentGame() {
        if (isWinner(PlayerPosition.TOP)) return PlayerPosition.TOP;
        if (isWinner(PlayerPosition.BOTTOM)) return PlayerPosition.BOTTOM;
        return null;
    }

    private boolean isWinner(@NonNull PlayerPosition position) {
        switch (mStyle){
            case DEATH_MATCH:
            case LIFE_DRAGGING:
                switch (position) {
                    case TOP:
                        return getPlayerInfo(PlayerPosition.BOTTOM).getCountOfLives() < 0;
                    case BOTTOM:
                        return getPlayerInfo(PlayerPosition.TOP).getCountOfLives() < 0;
                }
            case POINTS:
                switch (position) {
                    case TOP:
                        return getPlayerInfo(PlayerPosition.TOP).getCountOfPoints() >= MAX_POINT_NUMBER;
                    case BOTTOM:
                        return getPlayerInfo(PlayerPosition.BOTTOM).getCountOfPoints() >= MAX_POINT_NUMBER;
                }
            case INFINITY:
            default:
                return false;
        }
    }

    private void reset() {
        mPlayerTop.reset();
        mPlayerBottom.reset();
    }

    public LocalPlayerInfo getPlayerInfo(@NonNull PlayerPosition position) {
        switch (position) {
            case TOP:
                return mPlayerTop;
            case BOTTOM:
                return mPlayerBottom;
        }
        return null;
    }

    public void playerJustScored(PlayerPosition position) {
        getPlayerInfo(position).justScored();
        getPlayerInfo(position == PlayerPosition.TOP ? PlayerPosition.BOTTOM : PlayerPosition.TOP).justNotScored();
    }


    public static class LocalPlayerInfo extends PlayerInfo {
        private final PongPreferences.GamePlayType mStyle;

        protected LocalPlayerInfo(@NonNull PongPreferences.GamePlayType style) {
            mStyle = style;
            reset();
        }

        protected void reset() {
            mCountOfPoints = 0;
            mCountOfLives = LocalCurrentGameInfo.LIFE_NUMBER;
        }

        protected void justScored() {
            switch (mStyle) {
                case DEATH_MATCH:
                    break;
                case LIFE_DRAGGING:
                    mCountOfLives++;
                    mCountOfPoints++;
                    break;
                case POINTS:
                case INFINITY:
                    mCountOfPoints++;
                    break;
            }
        }

        protected void justNotScored() {
            switch (mStyle) {
                case DEATH_MATCH:
                case LIFE_DRAGGING:
                    mCountOfLives--;
                    break;
            }
        }
    }
}
