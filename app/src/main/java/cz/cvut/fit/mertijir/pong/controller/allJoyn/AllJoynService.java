/*
 * Copyright (c) 2010-2011,2013, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package cz.cvut.fit.mertijir.pong.controller.allJoyn;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusListener;
import org.alljoyn.bus.Mutable;
import org.alljoyn.bus.SessionListener;
import org.alljoyn.bus.SessionOpts;
import org.alljoyn.bus.SessionPortListener;
import org.alljoyn.bus.SignalEmitter;
import org.alljoyn.bus.Status;
import org.alljoyn.bus.annotation.BusSignalHandler;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.BuildConfig;
import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.model.communication.AJServiceSupportedSerializable;
import cz.cvut.fit.mertijir.pong.model.communication.DataRequest;
import cz.cvut.fit.mertijir.pong.model.communication.GameAction;
import cz.cvut.fit.mertijir.pong.model.communication.client.DefaultSurfaceProportion;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.GameStats;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

public class AllJoynService extends android.app.Service {
    private static final String TAG = "AllJoynService";
    private final Binder mLocalBinder = new Binder();
    private AllJoynHandler mHandler = null;
    /**
     * The state of the AllJoyn bus attachment.
     */
    private BusAttachmentState mBusAttachmentState = BusAttachmentState.DISCONNECTED;

    private Communicator.HostedChannel mHostedChannel;
    private Communicator.UsedChannel mUsedChannel;
    private final ArrayList<Communicator.FoundChannel> mFoundChannels = new ArrayList<>();

    private final ArrayList<ServiceListener> mListeners = new ArrayList<>();
    /**
     * The bus attachment is the object that provides AllJoyn services to Java
     * clients.  Pretty much all communiation with AllJoyn is going to go through
     * this obejct.
     */
    private final BusAttachment mBus = new BusAttachment("Pong", BusAttachment.RemoteMessage.Receive);
    /**
     * The well-known name prefix which all bus attachments hosting a channel
     * will use.  The NAME_PREFIX and the channel name are composed to give
     * the well-known name a hosting bus attachment will request and
     * advertise.
     */
    public static final String NAME_PREFIX = BuildConfig.APPLICATION_ID;
    /**
     * The well-known session port used as the contact port for the chat service.
     */
    private static final short CONTACT_PORT = 27;
    /**
     * The object path used to identify the service "location" in the bus
     * attachment.
     */
    private static final String OBJECT_PATH = "/Pong";
    /**
     * An instance of an AllJoyn bus listener that knows what to do with
     * foundAdvertisedName and lostAdvertisedName notifications.  Although
     * we often use the anonymous class idiom when talking to AllJoyn, the
     * bus listener works slightly differently and it is better to use an
     * explicitly declared class in this case.
     */
    private final ChannelListener mBusListener = new ChannelListener();

    private final PongCommunicationService mPongCommunicationService = new PongCommunicationService();


    static {
        Log.i(TAG, "System.loadLibrary(\"alljoyn_java\")");
        System.loadLibrary("alljoyn_java");
    }

    private boolean mUnableToJoinToHost;

    public void setUnableToJoinToHost(boolean unableToJoinToHost) {
        mUnableToJoinToHost = unableToJoinToHost;
    }


    /**
     * Interface of this service for Activities
     */
    public class Binder extends android.os.Binder {
        public AllJoynService getAllJoynService() {
            return AllJoynService.this;
        }
    }

    @Override
    public Binder onBind(Intent intent) {
        Log.i(TAG, "onBind()");
        return mLocalBinder;
    }

    @Override
    public void onCreate() {
        Log.i(TAG, "onCreate()");
        HandlerThread thread = new HandlerThread("AllJoynHandler");
        thread.start();
        mHandler = new AllJoynHandler(thread.getLooper(), this);
        mHandler.callAction(AllJoynHandler.Action.CONNECT);
        mHandler.callAction(AllJoynHandler.Action.START_DISCOVERY);
    }


    /**
     * Our onDestroy() is called by the Android appliation framework when it
     * decides that our service is no longer needed.  We tell our background
     * thread to exit andremove ourselves from the list of observers of the
     * model.
     */
    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
        mHandler.callAction(AllJoynHandler.Action.STOP_DISCOVERY);
        mHandler.callAction(AllJoynHandler.Action.DISCONNECT);
        mHandler.exit();
    }

    /**
     * The method onStartCommand() is called by the Android application
     * framework when a client explicitly starts a service by calling
     * startService().  We expect that the only place this is going to be done
     * is when the Android Application class for our application is created.
     * The Appliation class provides our model in the sense of the MVC
     * application we really are.
     * <p>
     * We return START_STICKY to enable us to be explicity started and stopped
     * which means that our service will essentially run "forever" (or until
     * Android decides that we should die for resource management issues) since
     * our Application class is left running as long as the process is left running.
     */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand()");
        return START_STICKY;
    }


    /**
     * Enumeration of the states of the AllJoyn bus attachment.  This
     * lets us make a note to ourselves regarding where we are in the process
     * of preparing and tearing down the fundamental connection to the AllJoyn
     * bus.
     * <p>
     * This should really be a more private think, but for the sample we want
     * to show the user the states we are running through.  Because we are
     * really making a data hiding exception, and because we trust ourselves,
     * we don't go to any effort to prevent the UI from changing our state out
     * from under us.
     * <p>
     * There are separate variables describing the states of the client
     * ("use") and service ("host") pieces.
     */
    public enum BusAttachmentState {
        /**
         * The bus attachment is not localJoinedToRemoteHost to the AllJoyn bus
         */
        DISCONNECTED,
        /**
         * The  bus attachment is localJoinedToRemoteHost to the AllJoyn bus
         */
        CONNECTED,
        /**
         * The bus attachment is discovering remote attachments hosting chat channels
         */
        DISCOVERING
    }


    private class ChannelListener extends BusListener {
        public void foundAdvertisedName(String nameWithPrefix, short transport, String namePrefix) {
            Log.i(TAG, "mBusListener.foundAdvertisedName(" + nameWithPrefix + ':' + transport + ")");
            Communicator.FoundChannel channel = getFoundChannel(nameWithPrefix);
            if (channel != null) return;
            channel = new Communicator.FoundChannel(namePrefix, nameWithPrefix, transport);
            channel.setState(Communicator.FoundChannel.FoundChannelState.ACTIVE);
            mFoundChannels.add(channel);
            Log.i(TAG, "addFoundChannel(): added " + channel);
            sendFoundChannelUpdate(channel);
        }

        public void lostAdvertisedName(String nameWithPrefix, short transport, String namePrefix) {
            Log.i(TAG, "mBusListener.lostAdvertisedName(" + nameWithPrefix + ':' + transport + ")");
            Communicator.FoundChannel channel = getFoundChannel(nameWithPrefix);
            if (channel == null) return;
            if (channel.isJoined()) {
                callLeaveSession();
            }
            channel.setState(Communicator.FoundChannel.FoundChannelState.LOST);
            mFoundChannels.remove(channel);
            sendFoundChannelUpdate(channel);
        }
    }


    /**
     * Implementation of the functionality related to connecting our app
     * to the AllJoyn bus.  We expect that this method will only be called in
     * the context of the AllJoyn bus handler thread; and while we are in the
     * DISCONNECTED state.
     */
    private void doConnect() {
        Log.i(TAG, "doConnect()");
        org.alljoyn.bus.alljoyn.DaemonInit.PrepareDaemon(getApplicationContext());
        if (mBusAttachmentState != BusAttachmentState.DISCONNECTED) return;
        mBus.useOSLogging(true);
        mBus.setDebugLevel("ALLJOYN_JAVA", 7);
        mBus.registerBusListener(mBusListener);

        /* 
         * To make a service available to other AllJoyn peers, first
         * register a BusObject with the BusAttachment at a specific
         * object path.  Our service is implemented by the PongCommunicationService
         * BusObject found at the "/chatService" object path.
         */
        Status status = mBus.registerBusObject(mPongCommunicationService, OBJECT_PATH);
        if (Status.OK != status) {
            Log.e(TAG, "Unable to register the chat bus object: (" + status + ")");
            return;
        }

        status = mBus.connect();
        if (status != Status.OK) {
            Log.e(TAG, "Unable to connect to the bus: (" + status + ")");
            return;
        }

        status = mBus.registerSignalHandlers(this);
        if (status != Status.OK) {
            Log.e(TAG, "Unable to register signal handlers: (" + status + ")");
            return;
        }

        mBusAttachmentState = BusAttachmentState.CONNECTED;
    }

    /**
     * Implementation of the functionality related to disconnecting our app
     * from the AllJoyn bus.  We expect that this method will only be called
     * in the context of the AllJoyn bus handler thread.  We expect that this
     * method will only be called in the context of the AllJoyn bus handler
     * thread; and while we are in the CONNECTED state.
     */
    private void doDisconnect() {
        Log.i(TAG, "doDisonnect()");
        if (mBusAttachmentState != BusAttachmentState.CONNECTED) return;
        mBus.unregisterBusListener(mBusListener);
        mBus.disconnect();
        mBusAttachmentState = BusAttachmentState.DISCONNECTED;
    }

    /**
     * Implementation of the functionality related to discovering remote apps
     * which are hosting chat channels.  We expect that this method will only
     * be called in the context of the AllJoyn bus handler thread; and while
     * we are in the CONNECTED state.  Since this is a core bit of functionalty
     * for the "use" side of the app, we always do this at startup.
     */
    private void doStartDiscovery() {
        Log.i(TAG, "doStartDiscovery()");
        if (mBusAttachmentState != BusAttachmentState.CONNECTED) return;
        Status status = mBus.findAdvertisedName(NAME_PREFIX);
        if (status == Status.OK) {
            mBusAttachmentState = BusAttachmentState.DISCOVERING;
        } else {
            Log.e(TAG, "Unable to start finding advertised names: (" + status + ")");
        }
    }

    /**
     * Implementation of the functionality related to stopping discovery of
     * remote apps which are hosting chat channels.
     */
    private void doStopDiscovery() {
        Log.i(TAG, "doStopDiscovery()");
        if (mBusAttachmentState != BusAttachmentState.CONNECTED) return;
        mBus.cancelFindAdvertisedName(NAME_PREFIX);
        mBusAttachmentState = BusAttachmentState.CONNECTED;
    }

    /**
     * Implementation of the functionality related to requesting a well-known
     * name from an AllJoyn bus attachment.
     */
    private void doRequestName(String name) {
        Log.i(TAG, "doRequestName()");

        /*
         * In order to request a name, the bus attachment must at least be
         * localJoinedToRemoteHost.
         */
        int stateRelation = mBusAttachmentState.compareTo(BusAttachmentState.DISCONNECTED);
        if (stateRelation < 0) return;

    	/*
         * We depend on the user interface and model to work together to not
    	 * get this process started until a valid name is set in the channel name.
    	 */
        String wellKnownName = NAME_PREFIX + "." + name;
        Status status = mBus.requestName(wellKnownName, BusAttachment.ALLJOYN_REQUESTNAME_FLAG_DO_NOT_QUEUE);
        if (status == Status.OK) {
            Communicator.HostedChannel channel = new Communicator.HostedChannel(NAME_PREFIX, name);
            channel.setState(Communicator.HostedChannel.HostChannelState.NAMED);
            mHostedChannel = channel;
            sendHostedChannelUpdate(channel);
        } else {
            Log.e(TAG, "Unable to acquire well-known name: (" + status + ")");
        }
    }

    /**
     * Implementation of the functionality related to releasing a well-known
     * name from an AllJoyn bus attachment.
     */
    private void doReleaseName() {
        Log.i(TAG, "doReleaseName()");
        
        /*
         * In order to release a name, the bus attachment must at least be
         * localJoinedToRemoteHost.
         */
        int stateRelation = mBusAttachmentState.compareTo(BusAttachmentState.DISCONNECTED);
        if (stateRelation < 0) return;
        if (mBusAttachmentState == null
                || mBusAttachmentState == BusAttachmentState.DISCONNECTED) return;
        /*
         * We need to progress monotonically down the hosted channel states
    	 * for sanity.
    	 */
        //assert (mHostChannelState == HostChannelState.NAMED);

    	/*
         * We depend on the user interface and model to work together to not
    	 * change the name out from under us while we are running.
    	 */
        //String wellKnownName = NAME_PREFIX + "." + name;

    	/*
         * There's not a lot we can do if the bus attachment refuses to release
    	 * the name.  It is not a fatal error, though, if it doesn't.  This is
    	 * because bus attachments can have multiple names.
    	 */
        Communicator.HostedChannel channel = getHostedChannel();
        if (channel == null || Communicator.HostedChannel.HostChannelState.NAMED != channel.getState())
            return;
        mBus.releaseName(channel.getWellKnownName());
        channel.setState(Communicator.HostedChannel.HostChannelState.IDLE);
        sendHostedChannelUpdate(channel);
        mHostedChannel = null;
    }

    /**
     * Implementation of the functionality related to binding a session port
     * to an AllJoyn bus attachment.
     */
    private void doBindSession() {
        Log.i(TAG, "doBindSession()");

        final Communicator.HostedChannel channel = getHostedChannel();
        if (channel == null || Communicator.HostedChannel.HostChannelState.NAMED != channel.getState())
            return;


        Mutable.ShortValue contactPort = new Mutable.ShortValue(CONTACT_PORT);
        channel.setSessionPort(contactPort.value);
        SessionOpts sessionOpts = new SessionOpts(SessionOpts.TRAFFIC_MESSAGES, true, SessionOpts.PROXIMITY_ANY, SessionOpts.TRANSPORT_ANY);

        Status status = mBus.bindSessionPort(contactPort, sessionOpts, new SessionPortListener() {
            /**
             * This method is called when a client tries to join the session
             * we have bound.  It asks us if we want to accept the client into
             * our session.
             *
             * In the class documentation for the SessionPortListener note that
             * it is a requirement for this method to be multithread safe.
             * Since we never access any shared state, this requirement is met.
             */
            public boolean acceptSessionJoiner(short sessionPort, String joiner, SessionOpts sessionOpts) {
                Log.i(TAG, "SessionPortListener.acceptSessionJoiner(" + sessionPort + ", " + joiner + ", " + sessionOpts.toString() + ")");
                if (mUnableToJoinToHost) {
                    return false;
                }
                /*
                 * Accept anyone who can get our contact port correct.
        		 */
                return (sessionPort == CONTACT_PORT);
            }

            /**
             * If we return true in acceptSessionJoiner, we admit a new client
             * into our session.  The session does not really exist until a 
             * client joins, at which time the session is created and a session
             * ID is assigned.  This method communicates to us that this event
             * has happened, and provides the new session ID for us to use.
             *
             * In the class documentation for the SessionPortListener note that
             * it is a requirement for this method to be multithread safe.
             * Since we never access any shared state, this requirement is met.
             *
             * See comments in joinSession for why the hosted chat interface is
             * created here. 
             */
            public void sessionJoined(short sessionPort, int sessionId, String joinerName) {
                Log.i(TAG, "SessionPortListener.sessionJoined(" + sessionPort + ", " + sessionId + ", " + joinerName + ")");
                SignalEmitter emitter = new SignalEmitter(mPongCommunicationService, sessionId, SignalEmitter.GlobalBroadcast.Off);
                IPongCommunication anInterface = emitter.getInterface(IPongCommunication.class);
                final Communicator.HostedChannel.Joiner joiner = new Communicator.HostedChannel.Joiner(sessionPort, sessionId, joinerName, anInterface);
                joiner.setActive(true);
                channel.addJoiner(joiner);
                mBus.setSessionListener(sessionId, new SessionListener() {
                    @Override
                    public void sessionMemberRemoved(int sessionId, String uniqueName) {
                        //// FIXME: 20.1.17 asdfasdfaseeefesf
                        channel.removeJoiner(joiner);
                        joiner.setActive(false);
                        sendJoinerToHostUpdate(joiner);
                    }
                });
                sendJoinerToHostUpdate(joiner);
            }
        });

        if (status == Status.OK) {
            channel.setState(Communicator.HostedChannel.HostChannelState.BOUND);
            sendHostedChannelUpdate(channel);//App.getAJCommunicator().setHostChannelState(mHostChannelState);
        } else {
            Log.e(TAG, "Unable to bind session contact port: (" + status + ")");
        }
    }

    /**
     * Implementation of the functionality related to un-binding a session port
     * from an AllJoyn bus attachment.
     */
    private void doUnbindSession() {
        Log.i(TAG, "doUnbindSession()");
        Communicator.HostedChannel channel = getHostedChannel();
        if (channel == null || Communicator.HostedChannel.HostChannelState.BOUND != channel.getState())
            return;
        /*
         * There's not a lot we can do if the bus attachment refuses to unbind
         * our port.
         */
        channel.dropJoiners();
        mBus.unbindSessionPort(channel.getSessionPort());
        channel.setState(Communicator.HostedChannel.HostChannelState.NAMED);
        sendHostedChannelUpdate(channel);//App.getAJCommunicator().setHostChannelState(mHostChannelState);
    }

    /**
     * Implementation of the functionality related to advertising a service on
     * an AllJoyn bus attachment.
     */
    private void doAdvertise() {
        Log.i(TAG, "doAdvertise()");
        
       	/*
         * We depend on the user interface and model to work together to not
    	 * change the name out from under us while we are running.
    	 */
        Communicator.HostedChannel channel = getHostedChannel();
        if (channel == null || Communicator.HostedChannel.HostChannelState.BOUND != channel.getState())
            return;

        Status status = mBus.advertiseName(channel.getWellKnownName(), SessionOpts.TRANSPORT_ANY);

        if (status != Status.OK) {
            Log.e(TAG, "Unable to advertise well-known name: (" + status + ")");
            return;
        }
        channel.setState(Communicator.HostedChannel.HostChannelState.ADVERTISED);
        sendHostedChannelUpdate(channel);//App.getAJCommunicator().setHostChannelState(mHostChannelState);
    }

    /**
     * Implementation of the functionality related to canceling an advertisement
     * on an AllJoyn bus attachment.
     */
    private void doCancelAdvertise() {
        Log.i(TAG, "doCancelAdvertise()");
        
       	/*
         * We depend on the user interface and model to work together to not
    	 * change the name out from under us while we are running.
    	 */
        Communicator.HostedChannel channel = getHostedChannel();
        if (channel == null || Communicator.HostedChannel.HostChannelState.ADVERTISED != channel.getState())
            return;

        Status status = mBus.cancelAdvertiseName(channel.getWellKnownName(), SessionOpts.TRANSPORT_ANY);

        if (status != Status.OK) {
            Log.e(TAG, "Unable to cancel advertisement of well-known name: (" + status + ")");
        }
        
        /*
         * There's not a lot we can do if the bus attachment refuses to cancel
         * our advertisement, so we don't bother to even get the status.
         */
        channel.setState(Communicator.HostedChannel.HostChannelState.BOUND);
        sendHostedChannelUpdate(channel);//App.getAJCommunicator().setHostChannelState(mHostChannelState);
    }

    /**
     * Implementation of the functionality related to joining an existing
     * local or remote session.
     */
    private void doJoinSession(@NonNull String name) {
        Log.i(TAG, "doJoinSession()");
        
        /*
         * There is a relatively non-intuitive behavior of multipoint sessions
         * that one needs to grok in order to understand the code below.  The
         * important thing to uderstand is that there can be only one endpoint
         * for a multipoint session in a particular bus attachment.  This
         * endpoint can be created explicitly by a call to joinSession() or
         * implicitly by a call to bindSessionPort().  An attempt to call
         * joinSession() on a session port we have created with bindSessionPort()
         * will result in an error.
         * 
         * When we call bindSessionPort(), we do an implicit joinSession() and
         * thus signals (which correspond to our chat messages) will begin to
         * flow from the hosted chat channel as soon as we begin to host a
         * corresponding session.
         * 
         * To achieve sane user interface behavior, we need to block those
         * signals from the implicit join done by the bind until our user joins
         * the bound chat channel.  If we do not do this, the chat messages
         * from the chat channel hosted by the application will appear in the
         * chat channel joined by the application.
         *
         * Since the messages flow automatically, we can accomplish this by
         * turning a filter on and off in the chat signal handler.  So if we
         * detect that we are hosting a channel, and we find that we want to
         * join the hosted channel we turn the filter off.
         * 
         * We also need to be able to send chat messages to the hosted channel.
         * This means we need to point the mChatInterface at the session ID of
         * the hosted session.  There is another complexity here since the
         * hosted session doesn't exist until a remote session has joined.
         * This means that we don't have a session ID to use to create a
         * SignalEmitter until a remote device does a joinSession on our
         * hosted session.  This, in turn, means that we have to create the
         * SignalEmitter after we get a sessionJoined() callback in the 
         * SessionPortListener passed into bindSessionPort().  We chose to
         * create the signal emitter for this case in the sessionJoined()
         * callback itself.  Note that this hosted channel signal emitter
         * must be distinct from one constructed for the usual joinSession
         * since a hosted channel may have a remote device do a join at any
         * time, even when we are joined to another session.  If they were
         * not separated, a remote join on the hosted session could redirect
         * messages from the joined session unexpectedly.
         * 
         * So, to summarize, these next few lines handle a relatively complex
         * case.  When we host a chat channel, we do a bindSessionPort which
         * *enables* the creation of a session.  As soon as a remote device
         * joins the hosted chat channel, a session is actually created, and
         * the SessionPortListener sessionJoined() callback is fired.  At that
         * point, we create a separate SignalEmitter using the hosted session's
         * sessionId that we can use to send chat messages to the channel we
         * are hosting.  As soon as the session comes up, we begin receiving
         * chat messages from the session, so we need to filter them until the
         * user joins the hosted chat channel.  In a separate timeline, the
         * user can decide to join the chat channel she is hosting.  She can
         * do so either before or after the corresponding session has been
         * created as a result of a remote device joining the hosted session. 
         * If she joins the hosted channel before the underlying session is
         * created, her chat messages will be discarded.  If she does so after
         * the underlying session is created, there will be a session emitter
         * waiting to use to send chat messages.  In either case, the signal
         * filter will be turned off in order to listen to remote chat
         * messages.
         */
        Communicator.FoundChannel foundChannel = getFoundChannel(AllJoynService.NAME_PREFIX + '.' + name);
        if (foundChannel == null) return;
        foundChannel.setJoined(true);

        Communicator.HostedChannel hostedChannel = getHostedChannel();
        if (hostedChannel != null) {
            hostedChannel.setJoinedToSelf(true);
            Communicator.UsedChannel usedChannel = getUsedChannel();
            if (usedChannel == null) {
                usedChannel = new Communicator.UsedChannel(hostedChannel);
                mUsedChannel = usedChannel;
            }
            usedChannel.setState(Communicator.UsedChannel.UseChannelState.JOINED);
            sendUsedChannelUpdate(usedChannel);
            return;
        }
           /*
         * We depend on the user interface and model to work together to provide
    	 * a reasonable name.
    	 */
        Communicator.UsedChannel channel = getUsedChannel();
        if (channel == null) {
            channel = new Communicator.UsedChannel(NAME_PREFIX, name);
            mUsedChannel = channel;
        }
        final Communicator.UsedChannel usedChannel = channel;
        /*
         * Since we can act as the host of a channel, we know what the other
         * side is expecting to see.
         */
        short contactPort = CONTACT_PORT;
        SessionOpts sessionOpts = new SessionOpts(SessionOpts.TRAFFIC_MESSAGES, true, SessionOpts.PROXIMITY_ANY, SessionOpts.TRANSPORT_ANY);
        Mutable.IntegerValue sessionId = new Mutable.IntegerValue();

        Status status = mBus.joinSession(usedChannel.getWellKnownName(), contactPort, sessionId, sessionOpts, new SessionListener() {
            /**
             * This method is called when the last remote participant in the 
             * chat session leaves for some reason and we no longer have anyone
             * to chat with.
             *
             * In the class documentation for the BusListener note that it is a
             * requirement for this method to be multithread safe.  This is
             * accomplished by the use of a monitor on the ChatApplication as
             * exemplified by the synchronized attribute of the removeFoundChannel
             * method there.
             */
            public void sessionLost(int sessionId, int reason) {
                Log.i(TAG, "BusListener.sessionLost(sessionId=" + sessionId + ",reason=" + reason + ")");
                Log.d(TAG, "The chat session has been lost");
                usedChannel.setState(Communicator.UsedChannel.UseChannelState.IDLE);
                sendUsedChannelUpdate(usedChannel);
                mUsedChannel = null;
            }
        });

        if (status == Status.OK) {
            Log.i(TAG, "doJoinSession(): use sessionId is " + sessionId.value);
            usedChannel.setSessionId(sessionId.value);
        } else {
            Log.d(TAG, "Unable to join chat session: (" + status + ")");
            return;
        }

        SignalEmitter emitter = new SignalEmitter(mPongCommunicationService, usedChannel.getSessionId(), SignalEmitter.GlobalBroadcast.Off);
        IPongCommunication anInterface = emitter.getInterface(IPongCommunication.class);
        usedChannel.setPongCommunicator(anInterface);

        usedChannel.setState(Communicator.UsedChannel.UseChannelState.JOINED);
        sendUsedChannelUpdate(usedChannel);
    }

    /**
     * Implementation of the functionality related to joining an existing
     * remote session.
     */
    private void doLeaveSession() {
        Communicator.UsedChannel usedChannel = getUsedChannel();
        if (usedChannel == null) return;
        Log.i(TAG, "doLeaveSession()");
        Communicator.HostedChannel hostedChannel = getHostedChannel();
        if (hostedChannel == null || !hostedChannel.getJoinedToSelf()) {
            mBus.leaveSession(usedChannel.getSessionId());
        }
        if (hostedChannel != null) {
            hostedChannel.setJoinedToSelf(false);
        }
        usedChannel.setState(Communicator.UsedChannel.UseChannelState.IDLE);
        sendUsedChannelUpdate(usedChannel);
        mUsedChannel = null;
        Communicator.FoundChannel foundChannel = getFoundChannel(usedChannel.getWellKnownName());
        if (foundChannel != null) {
            foundChannel.setJoined(false);
        }
    }

    public void callRequestName(String name) {
        mHandler.callAction(AllJoynHandler.Action.REQUEST_NAME, name);
    }

    public void callReleaseName() {
        mHandler.callAction(AllJoynHandler.Action.RELEASE_NAME);
    }

    public void callBindSession() {
        mHandler.callAction(AllJoynHandler.Action.BIND_SESSION);
    }

    public void callUnbindSession() {
        mHandler.callAction(AllJoynHandler.Action.UNBIND_SESSION);
    }

    public void callAdvertise() {
        mHandler.callAction(AllJoynHandler.Action.ADVERTISE);
    }

    public void callCancelAdvertise() {
        mHandler.callAction(AllJoynHandler.Action.CANCEL_ADVERTISE);
    }

    public void callJoinSession(String name) {
        mHandler.callAction(AllJoynHandler.Action.JOIN_SESSION, name);
    }

    public void callLeaveSession() {
        mHandler.callAction(AllJoynHandler.Action.LEAVE_SESSION);
    }

    /**
     * Our chat messages are going to be Bus Signals multicast out onto an
     * associated session.  In order to send signals, we need to define an
     * AllJoyn bus object that will allow us to instantiate a signal emmiter.
     * <p>
     * Intentionally empty implementation of methods.  Since this
     * method is only used as a signal emitter, it will never be called
     * directly.
     */
    class PongCommunicationService implements IPongCommunication {
        @Override
        public void sendSurfacePreparation(String surfacePreparation) throws BusException {
        }

        @Override
        public void sendObjectsPosition(String objectsPosition) throws BusException {
        }

        @Override
        public void sendPlayersEvent(String playersEvent) throws BusException {
        }

        @Override
        public void sendDefaultSurfaceProportion(String defaultSurfaceProportions) throws BusException {
        }

        @Override
        public void sendDataRequest(String dataRequest) throws BusException {
        }

        @Override
        public void sendGameAction(String gameActionRequest) throws BusException {
        }

        @Override
        public void sendGameStats(String playersStats) throws BusException {
        }
    }

    @Nullable
    private Communicator.HostedChannel getHostedChannel() {
        return mHostedChannel;
    }

    @Nullable
    private Communicator.UsedChannel getUsedChannel() {
        return mUsedChannel;
    }

    @Nullable
    private Communicator.FoundChannel getFoundChannel(String nameWithPrefix) {
        for (Communicator.FoundChannel channel : mFoundChannels) {
            if (channel.getWellKnownName().equals(nameWithPrefix)) {
                return channel;
            }
        }
        return null;
    }

    public void addListener(ServiceListener listener) {
        if (listener == null) return;
        mListeners.add(listener);
    }

    public void removeListener(ServiceListener listener) {
        if (listener == null) return;
        mListeners.remove(listener);
    }

    private void sendHostedChannelUpdate(Communicator.HostedChannel channel) {
        for (ServiceListener listener : mListeners) {
            listener.handleLocalHostUpdate(channel);
        }
    }

    private void sendUsedChannelUpdate(Communicator.UsedChannel usedChannel) {
        for (ServiceListener listener : mListeners) {
            listener.handleLocalJoinToRemoteHostUpdate(usedChannel);
        }
    }

    private void sendFoundChannelUpdate(Communicator.FoundChannel foundChannel) {
        for (ServiceListener listener : mListeners) {
            listener.handleFoundRemoteHostUpdate(foundChannel);
        }
    }

    private void sendJoinerToHostUpdate(Communicator.HostedChannel.Joiner joinedToHost) {
        for (ServiceListener listener : mListeners) {
            listener.handleRemoteJoinToLocalHostUpdate(joinedToHost);
        }
    }


    private static final class AllJoynHandler extends Handler {
        private static final int EXIT = -1;
        private static final Action[] ACTIONS = Action.values();
        private final AllJoynService mService;

        private AllJoynHandler(Looper looper, AllJoynService service) {
            super(looper);
            mService = service;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == EXIT) {
                getLooper().quit();
                return;
            }
            Action action;
            try { //if (msg.what < 0 && msg.what >= ACTIONS.length) return;
                action = ACTIONS[msg.what];
            } catch (IndexOutOfBoundsException e) {
                return;
            }
            Log.d(TAG, "handle " + action.name() + " " + msg.obj);
            if (msg.obj instanceof String) {
                String name = (String) msg.obj;
                switch (action) {
                    case REQUEST_NAME:
                        mService.doRequestName(name);
                        break;
                    case JOIN_SESSION:
                        mService.doJoinSession(name);
                        break;
                }
                return;
            }
            if (msg.obj instanceof AJServiceSupportedSerializable && action == Action.SEND_OBJECT) {
                AJServiceSupportedSerializable serializable = (AJServiceSupportedSerializable) msg.obj;
                mService.doSendObject(serializable);
                return;
            }
            switch (action) {
                case CONNECT:
                    mService.doConnect();
                    break;
                case DISCONNECT:
                    mService.doDisconnect();
                    break;
                case START_DISCOVERY:
                    mService.doStartDiscovery();
                    break;
                case STOP_DISCOVERY:
                    mService.doStopDiscovery();
                    break;
                case BIND_SESSION:
                    mService.doBindSession();
                    break;
                case UNBIND_SESSION:
                    mService.doUnbindSession();
                    break;
                case LEAVE_SESSION:
                    mService.doLeaveSession();
                    break;
                case RELEASE_NAME:
                    mService.doReleaseName();
                    break;
                case ADVERTISE:
                    mService.doAdvertise();
                    break;
                case CANCEL_ADVERTISE:
                    mService.doCancelAdvertise();
                    break;
            }
        }

        private void callAction(@NonNull AllJoynHandler.Action action) {
            sendMessage(obtainMessage(action.ordinal()));
        }

        private void callAction(@NonNull AllJoynHandler.Action action, String channelName) {
            sendMessage(obtainMessage(action.ordinal(), channelName));
        }

        private void callAction(@NonNull AJServiceSupportedSerializable object) {
            sendMessage(obtainMessage(Action.SEND_OBJECT.ordinal(), object));
        }

        private void exit() {
            sendMessage(this.obtainMessage(EXIT));
        }

        /**
         * Created by Jiří Mertin on 30.12.16.
         */
        private enum Action {
            CONNECT,
            DISCONNECT,

            START_DISCOVERY,
            STOP_DISCOVERY,

            REQUEST_NAME,
            RELEASE_NAME,

            BIND_SESSION,
            UNBIND_SESSION,

            ADVERTISE,
            CANCEL_ADVERTISE,

            JOIN_SESSION,
            LEAVE_SESSION,

            SEND_OBJECT
        }
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendDefaultSurfaceProportion")
    public void receiveDefaultSurfaceProportion(String string) {
        DefaultSurfaceProportion object = Serializer.deserialize(string, DefaultSurfaceProportion.class);
        receivedObject(object);
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendPlayersEvent")
    public void receivePlayersEvent(String string) {
        PlayersEvent object = Serializer.deserialize(string, PlayersEvent.class);
        receivedObject(object);
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendObjectsPosition")
    public void receiveObjectsPosition(String string) {
        ObjectsPosition object = Serializer.deserialize(string, ObjectsPosition.class);
        receivedObject(object);
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendSurfacePreparation")
    public void receiveSurfacePreparation(String string) {
        SurfacePreparation surfacePreparation = Serializer.deserialize(string, SurfacePreparation.class);
        receivedObject(surfacePreparation);
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendDataRequest")
    public void receiveDataRequest(String string) {
        DataRequest dataReceived = Serializer.deserialize(string, DataRequest.class);
        receivedObject(dataReceived);
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendGameAction")
    public void receiveGameActionRequest(String string) {
        GameAction gameAction = Serializer.deserialize(string, GameAction.class);
        receivedObject(gameAction);
    }

    @BusSignalHandler(iface = BuildConfig.APPLICATION_ID, signal = "sendGameStats")
    public void receivePlayersStats(String string) {
        GameStats gameAction = Serializer.deserialize(string, GameStats.class);
        receivedObject(gameAction);
    }

    public void callSendObject(@NonNull AJServiceSupportedSerializable serializable) {
        mHandler.callAction(serializable);
    }

    private void doSendObject(@NonNull AJServiceSupportedSerializable serializable) {
        Communicator.UsedChannel usedChannel = getUsedChannel();
        if (usedChannel != null) {
            serializable.sendThrough(usedChannel.getPongCommunicator());
        }
        Communicator.HostedChannel hostedChannel = getHostedChannel();
        if (hostedChannel == null) return;
        for (Communicator.HostedChannel.Joiner joiner : hostedChannel.getJoiners()) {
            serializable.sendThrough(joiner.getPongCommunicator());
        }
    }

    private void receivedObject(AJServiceSupportedSerializable serializable) {
        for (ServiceListener listener : mListeners) {
            listener.handleReceivedObject(serializable);
        }
    }

    /**
     * Created by Jiří Mertin on 15.1.17.
     */
    public interface ServiceListener {
        void handleLocalHostUpdate(Communicator.HostedChannel hostedChannel);

        void handleLocalJoinToRemoteHostUpdate(Communicator.UsedChannel usedChannel);

        void handleFoundRemoteHostUpdate(Communicator.FoundChannel foundChannel);

        void handleRemoteJoinToLocalHostUpdate(Communicator.HostedChannel.Joiner joinedToHost);

        void handleReceivedObject(AJServiceSupportedSerializable serializable);
    }
}
