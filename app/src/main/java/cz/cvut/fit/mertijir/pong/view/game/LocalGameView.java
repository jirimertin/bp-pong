package cz.cvut.fit.mertijir.pong.view.game;

import android.content.Context;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;
import cz.cvut.fit.mertijir.pong.model.game.bat.Bat;
import cz.cvut.fit.mertijir.pong.model.game.bat.WallBat;

/**
 * Created by Jiří Mertin on 10.1.17.
 */

public class LocalGameView extends GameView {
    private IPlayerWon mIPlayerWon;
    protected final ArrayList<LocalBall> mBalls = new ArrayList<>(2);
    protected final ArrayList<WallBat> mBats = new ArrayList<>();

    public LocalGameView(Context context) {
        super(context);
        if (context instanceof IPlayerWon) {
            mIPlayerWon = (IPlayerWon) context;
        }
    }

    @Override
    protected void createGameObjects() {
        PongPreferences.BatControlType upper1 = PongPreferences.getGameControlUpper1Type();
        PongPreferences.BatControlType upper2 = PongPreferences.getGameControlUpper2Type();
        PongPreferences.BatControlType bottom1 = PongPreferences.getGameControlBottom1Type();
        PongPreferences.BatControlType bottom2 = PongPreferences.getGameControlBottom2Type();
        boolean upper2isActive = PongPreferences.isUpperSecondaryBat();
        boolean bottom2isActive = PongPreferences.isBottomSecondaryBat();

        LocalProportions.Position upperPosition = getWidth() > getHeight() ? LocalProportions.Position.LEFT : LocalProportions.Position.TOP;
        LocalProportions.Position bottomPosition = getWidth() > getHeight() ? LocalProportions.Position.RIGHT : LocalProportions.Position.BOTTOM;

        mBats.add(createLocalBat(upper1, upperPosition, PongPreferences.BatPosition.UPPER1));
        mBats.add(createLocalBat(bottom1, bottomPosition, PongPreferences.BatPosition.BOTTOM1));
        if (upper2isActive) {
            mBats.add(createLocalBat(upper2, upperPosition, PongPreferences.BatPosition.UPPER2));
        }
        if (bottom2isActive) {
            mBats.add(createLocalBat(bottom2, bottomPosition, PongPreferences.BatPosition.BOTTOM2));
        }

        mWallBatDecision = new LocalProportions.WallBatDecision(mBats);

        recreateBalls();
    }

    protected void recreateBalls() {
        for (int i = getBalls().size() - 1; i > -1; i--) {
            getBalls().get(i).setRunning(false);
            getBalls().remove(getBalls().get(i));
        }
        getBalls().add(new LocalBall(mWallBatDecision));
        if (PongPreferences.isSecondaryBall()) {
            getBalls().add(new LocalBall(getBalls().get(0)));
        }
    }

    @Override
    public void update() {
        mEventValues.saveBalls(getBalls());
        for (Bat bat : mBats) {
            bat.move(mEventValues);
        }
        for (LocalBall ball : getBalls()) {
            ball.checkWallBatCollision();
        }

        ArrayList<LocalProportions.Position> winners = new ArrayList<>(2);
        for (LocalBall ball : getBalls()) {
            LocalProportions.Position positionOfTheBall = ball.checkBounds(getWidth(), getHeight());
            if (positionOfTheBall != null) {
                switch (positionOfTheBall) {
                    case TOP:
                        winners.add(LocalProportions.Position.BOTTOM);
                        break;
                    case BOTTOM:
                        winners.add(LocalProportions.Position.TOP);
                        break;
                }
            }
            ball.update();
        }
        if (winners.size() > 0) {
            if (mIPlayerWon != null) {
                mIPlayerWon.playerWon(winners.toArray(new LocalProportions.Position[winners.size()]));
            }
            recreateBalls();
            if (mIPlayerWon != null && !mIPlayerWon.gameHasEnded()) {
                setRunningLoop(true);
            }
        }
    }

    @Override
    public ArrayList<WallBat> getBats() {
        return mBats;
    }

    public interface IPlayerWon {
        void playerWon(LocalProportions.Position... positions);

        boolean gameHasEnded();
    }

    @Override
    public ArrayList<LocalBall> getBalls() {
        return mBalls;
    }
}
