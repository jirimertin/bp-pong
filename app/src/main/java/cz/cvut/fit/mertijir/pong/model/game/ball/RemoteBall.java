package cz.cvut.fit.mertijir.pong.model.game.ball;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.ColorInt;

import org.apache.commons.math3.fraction.Fraction;

import cz.cvut.fit.mertijir.pong.App;

/**
 * Created by Jiří Mertin on 10.1.17.
 */

public class RemoteBall extends Ball {
    @ColorInt
    private int mColor;

    @Override
    public void update() {
    }

    @Override
    public void setRunning(boolean running) {
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(getColor());
        canvas.drawRect(mX, mY, mX + getSize(), mY + getSize(), paint);
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        mColor = color;
    }

    public void setPositionProportions(Fraction x, Fraction y) {
        mX = App.getLocalProportions().getWidth() * x.getNumerator() / x.getDenominator();
        mY = App.getLocalProportions().getHeight() * y.getNumerator() / y.getDenominator();
    }
}
