package cz.cvut.fit.mertijir.pong.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.R;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.controller.allJoyn.Communicator;
import cz.cvut.fit.mertijir.pong.controller.allJoyn.CommunicatorListener;
import cz.cvut.fit.mertijir.pong.model.communication.DataRequest;
import cz.cvut.fit.mertijir.pong.model.communication.DataType;
import cz.cvut.fit.mertijir.pong.model.communication.GameAction;
import cz.cvut.fit.mertijir.pong.model.communication.client.DefaultSurfaceProportion;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.GameStats;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.communication.server.SurfacePreparation;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.LocalRealSurfaceSize;
import cz.cvut.fit.mertijir.pong.view.game.GameActivity;
import cz.cvut.fit.mertijir.pong.view.settings.BottomPlayerPreferencesActivity;
import cz.cvut.fit.mertijir.pong.view.settings.GamePreferencesActivity;
import cz.cvut.fit.mertijir.pong.view.settings.UpperPlayerPreferencesActivity;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private static final String DIALOG_FRAGMENT = "dialog_fragment";

    @BindView(R.id.mainActivity_upperPlayerPreferences_tableRow)
    TableRow mUpperPadTableRow;
    @BindView(R.id.mainActivity_bottomPlayerPreferences_tableRow)
    TableRow mBottomPadTableRow;

    PadRow mUpperPads;
    PadRow mBottomPads;
    private MainActivityListener mMainActivityListener;

    static class PadRow {
        @BindView(R.id.linearLayout_padsPreview_primaryPad)
        LinearLayout mPrimaryPadLayout;
        @BindView(R.id.linearLayout_padsPreview_secondaryPad)
        LinearLayout mSecondaryPadLayout;

        final Pad mPrimaryPad;
        final Pad mSecondaryPad;

        PadRow(@NonNull TableRow padRow) {
            ButterKnife.bind(this, padRow);
            mPrimaryPad = new Pad(mPrimaryPadLayout);
            mSecondaryPad = new Pad(mSecondaryPadLayout);
        }
    }

    static class Pad {
        @BindView(R.id.textView_padPreview_padControl)
        TextView mPadControl;
        @BindView(R.id.textView_padPreview_padColor)
        TextView mPadColor;

        Pad(@NonNull LinearLayout pad) {
            ButterKnife.bind(this, pad);
        }
    }

    @BindView(R.id.mainActivity_screenSizeValue_textView)
    TextView mScreenSize;

    @BindView(R.id.mainActivity_gamePlayTypeValue_textView)
    TextView mGamePlay;
    @BindView(R.id.mainActivity_ballNumberValue_textView)
    TextView mNumberOfBalls;

    @BindView(R.id.mainActivity_remoteCreate_tableRow)
    TableRow mRemoteCreateRow;
    @BindView(R.id.mainActivity_remoteStop_tableRow)
    TableRow mRemoteStopRow;
    @BindView(R.id.mainActivity_remoteJoinRow_tableRow)
    TableRow mRemoteJoinRow;
    @BindView(R.id.mainActivity_remoteLeaveRow_tableRow)
    TableRow mRemoteLeaveRow;

    @BindView(R.id.mainActivity_remoteConnect_button)
    Button mButtonConnect;
    @BindView(R.id.mainActivity_yourHostnameValue_textView)
    TextView mHostname;
    @BindView(R.id.mainActivity_noHostAvailable_textView)
    TextView mAvailableHostsNo;
    @BindView(R.id.mainActivity_someHostAvailable_textView)
    TextView mAvailableHostsSome;
    @BindView(R.id.mainActivity_connectedToValue_textView)
    TextView mConnectedTo;

    @BindView(R.id.mainActivity_localGameStart_button)
    Button mStartGameButton;
    @BindView(R.id.mainActivity_remoteWaiting_textView)
    TextView mRemoteGameWaiting;
    @BindView(R.id.mainActivity_remoteGameStart_button)
    Button mRemoteGameButton;
    @BindView(R.id.mainActivity_sendDataAgain_button)
    Button mSendDataAgainButton;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_linear_layout);
        ButterKnife.bind(this);
        mUpperPads = new PadRow(mUpperPadTableRow);
        mBottomPads = new PadRow(mBottomPadTableRow);
        if (App.getLocalProportions() != null) {
            mScreenSize.setText(String.format(
                    Locale.getDefault(),
                    getString(R.string.mainActivity_screenSizeValue_textView),
                    App.getLocalProportions().getLocalRealSurfaceSize().getWidth(),
                    App.getLocalProportions().getLocalRealSurfaceSize().getHeight()));
        }

        mMainActivityListener = new MainActivityListener();
    }

    @OnClick(R.id.mainActivity_localGameStart_button)
    void startGameActivity() {
        Intent gameActivityIntent = new Intent(this, GameActivity.class);
        startActivity(gameActivityIntent);
    }

    @OnClick(R.id.mainActivity_remoteGameStart_button)
    void remoteGameClick() {
        makeConnectionAction(MainActivityConnectingAction.HOSTED_USER_STARTED_SEND_SURFACE_PREPARATION);
    }

    @OnClick(R.id.mainActivity_editPreferences_button)
    void startGameSettings() {
        startActivity(new Intent(this, GamePreferencesActivity.class));
    }

    @OnClick(R.id.mainActivity_upperPlayer_button)
    void startUpperPadSettings() {
        startActivity(new Intent(this, UpperPlayerPreferencesActivity.class));
    }

    @OnClick(R.id.mainActivity_bottomPlayer_button)
    void startBottomPadSettings() {
        startActivity(new Intent(this, BottomPlayerPreferencesActivity.class));
    }

    @OnClick(R.id.mainActivity_remoteCreate_button)
    void createClick() {
        Communicator communicator = App.getAJCommunicator();
        String name = Integer.toHexString(App.getRandom().nextInt());//"kocour"; //diakritika dela problem
        communicator.startChannel(name);
    }

    @OnClick(R.id.mainActivity_sendDataAgain_button)
    void sendDataAgainClick() {
        DataRequest dataRequest = new DataRequest(DataType.DEFAULT_SURFACE_PROPORTION);
        App.getAJCommunicator().sendDataRequest(dataRequest);
    }

    @OnClick(R.id.mainActivity_remoteConnect_button)
    void connectClick() {
        AlertDialog.Builder build = new AlertDialog.Builder(this);
        build.setTitle(R.string.mainActivity_dialogChannelSelect_title);
        final Communicator communicator = App.getAJCommunicator();
        final ArrayList<String> channels = communicator.getFoundChannels();
        build.setItems(channels.toArray(new String[channels.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                communicator.joinChannel(channels.get(which));
            }
        }).show();
    }

    @OnClick(R.id.mainActivity_remoteStop_button)
    void stopClick() {
        Communicator communicator = App.getAJCommunicator();
        communicator.stopChannel();
    }

    @OnClick(R.id.mainActivity_remoteLeave_button)
    void leaveClick() {
        Communicator communicator = App.getAJCommunicator();
        communicator.leaveChannel();
    }

    @OnClick(R.id.mainActivity_remoteHelp_button)
    void helpClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.mainActivity_dialogConnectionHelp_title);
        builder.setMessage(R.string.mainActivity_dialogConnectionHelp_message);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        PongPreferences.BatControlType upper1 = PongPreferences.getGameControlUpper1Type();
        mUpperPads.mPrimaryPad.mPadControl.setText(PongPreferences.getGameControlTextFromType(upper1));
        mUpperPads.mPrimaryPad.mPadColor.setBackgroundColor(PongPreferences.getBatColor(PongPreferences.BatPosition.UPPER1));
        if (PongPreferences.isUpperSecondaryBat()) {
            mUpperPads.mSecondaryPadLayout.setVisibility(View.VISIBLE);
            PongPreferences.BatControlType upper2 = PongPreferences.getGameControlUpper2Type();
            mUpperPads.mSecondaryPad.mPadControl.setText(PongPreferences.getGameControlTextFromType(upper2));
            mUpperPads.mSecondaryPad.mPadColor.setBackgroundColor(PongPreferences.getBatColor(PongPreferences.BatPosition.UPPER2));
        } else {
            mUpperPads.mSecondaryPadLayout.setVisibility(View.INVISIBLE);
        }
        PongPreferences.BatControlType bottom1 = PongPreferences.getGameControlBottom1Type();
        mBottomPads.mPrimaryPad.mPadControl.setText(PongPreferences.getGameControlTextFromType(bottom1));
        mBottomPads.mPrimaryPad.mPadColor.setBackgroundColor(PongPreferences.getBatColor(PongPreferences.BatPosition.BOTTOM1));
        if (PongPreferences.isBottomSecondaryBat()) {
            mBottomPads.mSecondaryPadLayout.setVisibility(View.VISIBLE);
            PongPreferences.BatControlType bottom2 = PongPreferences.getGameControlBottom2Type();
            mBottomPads.mSecondaryPad.mPadControl.setText(PongPreferences.getGameControlTextFromType(bottom2));
            mBottomPads.mSecondaryPad.mPadColor.setBackgroundColor(PongPreferences.getBatColor(PongPreferences.BatPosition.BOTTOM2));
        } else {
            mBottomPads.mSecondaryPadLayout.setVisibility(View.INVISIBLE);
        }
        mGamePlay.setText(PongPreferences.getGamePlayTextFromType(PongPreferences.getGamePlayType()));
        mNumberOfBalls.setText((PongPreferences.isSecondaryBall()) ? getString(R.string.mainActivity_ballNumber_two) : getString(R.string.mainActivity_ballNumber_one));

        App.getAJCommunicator().addListener(mMainActivityListener);
        App.getAJCommunicator().sendListenerDataTo(mMainActivityListener);
        if (App.getAJCommunicator().getState() == Communicator.ConnectionState.HOSTED) {
            sendDataAgainClick();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.getAJCommunicator().removeListener(mMainActivityListener);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            View view = getWindow().findViewById(Window.ID_ANDROID_CONTENT);
            Log.d(TAG, "" + view.getRight() + " " + view.getBottom());
            if (view.getRight() == 0 || view.getBottom() == 0) {
                finish();
                return;
            }
            App.setLocalProportions(new LocalProportions(new LocalRealSurfaceSize(view.getRight(), view.getBottom())));
            if (mScreenSize != null) {
                mScreenSize.setText(getString(R.string.mainActivity_screenSizeValue_textView, view.getRight(), view.getBottom()));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Communicator communicator = App.getAJCommunicator();
        communicator.quit();
    }

    private void changeStateTo(final Communicator.ConnectionState state) {
        mSendDataAgainButton.setVisibility(View.GONE);
        switch (state) {
            case NONE:
                mRemoteCreateRow.setVisibility(View.VISIBLE);
                mRemoteStopRow.setVisibility(View.GONE);
                mRemoteJoinRow.setVisibility(View.VISIBLE);
                mRemoteLeaveRow.setVisibility(View.GONE);
                mStartGameButton.setVisibility(View.VISIBLE);
                mRemoteGameWaiting.setVisibility(View.GONE);
                mRemoteGameButton.setVisibility(View.GONE);
                break;
            case HOSTED:
                mRemoteCreateRow.setVisibility(View.GONE);
                mRemoteStopRow.setVisibility(View.VISIBLE);
                mRemoteJoinRow.setVisibility(View.GONE);
                mRemoteLeaveRow.setVisibility(View.GONE);
                mStartGameButton.setVisibility(View.GONE);
                mRemoteGameWaiting.setVisibility(View.VISIBLE);
                mRemoteGameWaiting.setText(R.string.mainActivity_remoteWaiting_waitingForJoiner);
                mRemoteGameButton.setVisibility(View.GONE);
                break;
            case JOINED:
                mRemoteCreateRow.setVisibility(View.GONE);
                mRemoteStopRow.setVisibility(View.GONE);
                mRemoteJoinRow.setVisibility(View.GONE);
                mRemoteLeaveRow.setVisibility(View.VISIBLE);
                mStartGameButton.setVisibility(View.GONE);
                mRemoteGameWaiting.setVisibility(View.VISIBLE);
                mRemoteGameWaiting.setText(R.string.mainActivity_remoteWaiting_waitingForHost);
                mRemoteGameButton.setVisibility(View.GONE);
                break;
        }
    }

    private class MainActivityListener implements CommunicatorListener {

        @Override
        public void localHostCreated(final boolean created, final @Nullable String name) {
            mHostname.post(new Runnable() {
                @Override
                public void run() {
                    changeStateTo(App.getAJCommunicator().getState());
                    if (created) {
                        mHostname.setText(name);
                    }
                }
            });
        }

        @Override
        public void remoteHostAvailability(final boolean available) {
            mAvailableHostsNo.post(new Runnable() {
                @Override
                public void run() {
                    if (available) {
                        mAvailableHostsSome.setVisibility(View.VISIBLE);
                        mButtonConnect.setVisibility(View.VISIBLE);
                        mAvailableHostsNo.setVisibility(View.INVISIBLE);
                    } else {
                        mAvailableHostsSome.setVisibility(View.INVISIBLE);
                        mButtonConnect.setVisibility(View.INVISIBLE);
                        mAvailableHostsNo.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        @Override
        public void localJoinedToRemoteHost(final boolean connected, final @Nullable String name) {
            mConnectedTo.post(new Runnable() {
                @Override
                public void run() {
                    changeStateTo(App.getAJCommunicator().getState());
                    if (connected) {
                        mConnectedTo.setText(name);
                    }
                }
            });
            if (connected) {
                makeConnectionAction(MainActivityConnectingAction.JOINED_SEND_LOCAL_SURFACE_PROPORTION);
            }
        }

        @Override
        public void remoteJoinedToLocalHost(final boolean has, @Nullable String joinerName) {
            mStartGameButton.post(new Runnable() {
                @Override
                public void run() {
                    changeStateTo(App.getAJCommunicator().getState());
                    mRemoteGameWaiting.setText(R.string.mainActivity_remoteWaiting_waitingForDataFromRemoteDevice);
                    mSendDataAgainButton.setVisibility(has ? View.VISIBLE : View.GONE);
                }
            });
        }

        @Override
        public void receivedDefaultSurfaceProportions(final DefaultSurfaceProportion remoteProportion) {
            switch (App.getAJCommunicator().getState()) {
                case HOSTED:
                    mStartGameButton.post(new Runnable() {
                        @Override
                        public void run() {
                            App.getLocalProportions().changeSizesByBestProportion(remoteProportion.mSurfaceProportion);
                            makeConnectionAction(MainActivityConnectingAction.HOSTED_WAIT_FOR_USER_START);
                        }
                    });
            }
        }

        @Override
        public void receivedPlayersEvents(PlayersEvent playersEvent) {
        }

        @Override
        public void receivedObjectPositions(ObjectsPosition objectsPosition) {
        }

        @Override
        public void receivedSurfacePreparation(SurfacePreparation surfacePreparation) {
            switch (App.getAJCommunicator().getState()) {
                case JOINED:
                    App.getInstance().setClientSurfacePreparation(surfacePreparation);
                    App.getLocalProportions().changeSizesByBestProportion(surfacePreparation.mSurfaceProportion);
                    makeConnectionAction(MainActivityConnectingAction.JOINED_START_ACTION_CONFIRM_AND_START_GAME);
            }
        }

        @Override
        public void cannotJoinWhenHosting() {
            mStartGameButton.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, R.string.mainActivity_communicationError_cannotJoinWhenHosting, Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void cannotHostWhenJoined() {
            mStartGameButton.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, R.string.mainActivity_communicationError_cannotHostWhenJoined, Toast.LENGTH_LONG).show();
                }
            });
        }

        @Override
        public void receivedDataRequest(DataRequest dataRequest) {
            switch (App.getAJCommunicator().getState()) {
                case JOINED:
                    if (dataRequest.mDataType == DataType.DEFAULT_SURFACE_PROPORTION) {
                        makeConnectionAction(MainActivityConnectingAction.JOINED_SEND_LOCAL_SURFACE_PROPORTION);
                    }
            }
        }

        @Override
        public void receivedGameAction(GameAction gameAction) {
            if (App.getAJCommunicator().getState() == Communicator.ConnectionState.HOSTED
                    && gameAction.mActionType == GameAction.ActionType.START_CONFIRMATION) {
                mStartGameButton.post(new Runnable() {
                    @Override
                    public void run() {
                        makeConnectionAction(MainActivityConnectingAction.HOSTED_START_GAME);
                    }
                });
            }
        }

        @Override
        public void receivedGameStats(GameStats gameStats) {
        }
    }

    private enum MainActivityConnectingAction {
        JOINED_SEND_LOCAL_SURFACE_PROPORTION,
        HOSTED_WAIT_FOR_USER_START,
        HOSTED_USER_STARTED_SEND_SURFACE_PREPARATION,
        JOINED_START_ACTION_CONFIRM_AND_START_GAME,
        HOSTED_START_GAME
    }

    private void makeConnectionAction(MainActivityConnectingAction action) {
        switch (action) {
            case JOINED_SEND_LOCAL_SURFACE_PROPORTION:
                DefaultSurfaceProportion proportion = new DefaultSurfaceProportion(App.getLocalProportions().getLocalRealSurfaceSize());
                App.getAJCommunicator().sendDefaultSurfaceProportion(proportion);
                break;
            case HOSTED_WAIT_FOR_USER_START:
                mStartGameButton.setVisibility(View.GONE);
                mRemoteGameButton.setVisibility(View.VISIBLE);
                mRemoteGameWaiting.setVisibility(View.GONE);
                mSendDataAgainButton.setVisibility(View.GONE);
                break;
            case HOSTED_USER_STARTED_SEND_SURFACE_PREPARATION:
                SurfacePreparation surfacePreparation = new SurfacePreparation(App.getLocalProportions(), PongPreferences.getGamePlayType());
                App.getAJCommunicator().sendSurfacePreparation(surfacePreparation);
                break;
            case JOINED_START_ACTION_CONFIRM_AND_START_GAME:
                GameAction gameAction = new GameAction(GameAction.ActionType.START_CONFIRMATION);
                App.getAJCommunicator().sendGameAction(gameAction);
                startGameActivity();
                break;
            case HOSTED_START_GAME:
                startGameActivity();
        }
    }
}
