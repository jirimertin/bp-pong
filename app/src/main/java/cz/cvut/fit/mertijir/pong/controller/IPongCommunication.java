package cz.cvut.fit.mertijir.pong.controller;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusObject;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusSignal;

import cz.cvut.fit.mertijir.pong.BuildConfig;

/**
 * Created by Jiří Mertin on 19.1.17.
 */

@BusInterface(name = BuildConfig.APPLICATION_ID)
public interface IPongCommunication extends BusObject {
    @BusSignal
    void sendSurfacePreparation(String surfacePreparation) throws BusException;

    @BusSignal
    void sendObjectsPosition(String objectsPosition) throws BusException;

    @BusSignal
    void sendPlayersEvent(String playersEvent) throws BusException;

    @BusSignal
    void sendDefaultSurfaceProportion(String defaultSurfaceProportions) throws BusException;

    @BusSignal
    void sendDataRequest(String dataRequest) throws BusException;

    @BusSignal
    void sendGameAction(String gameActionRequest) throws BusException;

    @BusSignal
    void sendGameStats(String playersStats) throws BusException;
}
