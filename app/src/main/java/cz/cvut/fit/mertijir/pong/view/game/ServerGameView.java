package cz.cvut.fit.mertijir.pong.view.game;

import android.content.Context;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.communication.server.ObjectsPosition;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;
import cz.cvut.fit.mertijir.pong.model.game.bat.ComputerWallBat;
import cz.cvut.fit.mertijir.pong.model.game.bat.WallBat;

/**
 * Created by Jiří Mertin on 23.1.17.
 */

public class ServerGameView extends GameView {
    private LocalGameView.IPlayerWon mIPlayerWon;
    protected final ArrayList<LocalBall> mBalls = new ArrayList<>(2);

    private final ArrayList<WallBat> mBats = new ArrayList<>();
    private final ArrayList<BatPlayer> mBatPlayers = new ArrayList<>();
    private PlayersEvent mRemotePlayersEvent;
    private ObjectsPosition mLocalObjectsPosition;

    public ServerGameView(Context context) {
        super(context);
        if (context instanceof LocalGameView.IPlayerWon) {
            mIPlayerWon = (LocalGameView.IPlayerWon) context;
        }
    }

    @Override
    protected void createGameObjects() {
        PongPreferences.BatControlType upper1 = PongPreferences.getGameControlUpper1Type();
        PongPreferences.BatControlType upper2 = PongPreferences.getGameControlUpper2Type();
        PongPreferences.BatControlType bottom1 = PongPreferences.getGameControlBottom1Type();
        PongPreferences.BatControlType bottom2 = PongPreferences.getGameControlBottom2Type();
        boolean upper2isActive = PongPreferences.isUpperSecondaryBat();
        boolean bottom2isActive = PongPreferences.isBottomSecondaryBat();

        LocalProportions.Position upperPosition = getWidth() > getHeight() ? LocalProportions.Position.LEFT : LocalProportions.Position.TOP;
        LocalProportions.Position bottomPosition = getWidth() > getHeight() ? LocalProportions.Position.RIGHT : LocalProportions.Position.BOTTOM;

        mBats.add(createLocalBat(upper1, upperPosition, PongPreferences.BatPosition.UPPER1));
        mBatPlayers.add(BatPlayer.REMOTE);
        mBats.add(createLocalBat(bottom1, bottomPosition, PongPreferences.BatPosition.BOTTOM1));
        mBatPlayers.add(BatPlayer.LOCAL);
        if (upper2isActive) {
            mBats.add(createLocalBat(upper2, upperPosition, PongPreferences.BatPosition.UPPER2));
            mBatPlayers.add(BatPlayer.REMOTE);
        }
        if (bottom2isActive) {
            mBats.add(createLocalBat(bottom2, bottomPosition, PongPreferences.BatPosition.BOTTOM2));
            mBatPlayers.add(BatPlayer.LOCAL);
        }

        mWallBatDecision = new LocalProportions.WallBatDecision(mBats);
        mLocalObjectsPosition = new ObjectsPosition();
        recreateBalls();
    }

    private void recreateBalls() {
        for (int i = getBalls().size() - 1; i > -1; i--) {
            getBalls().get(i).setRunning(false);
            getBalls().remove(getBalls().get(i));
        }
        getBalls().add(new LocalBall(mWallBatDecision));
        if (PongPreferences.isSecondaryBall()) {
            getBalls().add(new LocalBall(getBalls().get(0)));
        }
    }

    @Override
    public void update() {
        mEventValues.saveBalls(getBalls());
        for (int i = 0; i < mBats.size(); i++) {
            WallBat bat = mBats.get(i);
            if (mBatPlayers.get(i) == BatPlayer.LOCAL || bat instanceof ComputerWallBat) {
                bat.move(mEventValues);
            } else {
                bat.moveRemote(mRemotePlayersEvent);
            }
            mLocalObjectsPosition.setBat(bat, getWidth());
        }
        for (LocalBall ball : getBalls()) {
            ball.checkWallBatCollision();
        }

        ArrayList<LocalProportions.Position> winners = new ArrayList<>(2);
        for (LocalBall ball : getBalls()) {
            LocalProportions.Position positionOfTheBall = ball.checkBounds(getWidth(), getHeight());
            if (positionOfTheBall != null) {
                switch (positionOfTheBall) {
                    case TOP:
                        winners.add(LocalProportions.Position.BOTTOM);
                        break;
                    case BOTTOM:
                        winners.add(LocalProportions.Position.TOP);
                        break;
                }
            }
            ball.update();
            mLocalObjectsPosition.setBall(ball, getWidth(), getHeight());
        }
        if (winners.size() > 0) {
            if (mIPlayerWon != null) {
                mIPlayerWon.playerWon(winners.toArray(new LocalProportions.Position[winners.size()]));
            }
            recreateBalls();
            if (mIPlayerWon != null && !mIPlayerWon.gameHasEnded()) {
                setRunningLoop(true);
            }
        }
        App.getAJCommunicator().sendObjectsPosition(mLocalObjectsPosition);
    }

    @Override
    public ArrayList<LocalBall> getBalls() {
        return mBalls;
    }

    @Override
    public ArrayList<WallBat> getBats() {
        return mBats;
    }

    public void setRemotePlayersEvent(PlayersEvent remotePlayersEvent) {
        mRemotePlayersEvent = remotePlayersEvent;
    }

    private enum BatPlayer {
        LOCAL, REMOTE
    }
}
