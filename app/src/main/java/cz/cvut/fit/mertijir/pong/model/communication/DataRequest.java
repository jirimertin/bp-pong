package cz.cvut.fit.mertijir.pong.model.communication;

import android.support.annotation.NonNull;

import org.alljoyn.bus.BusException;

import cz.cvut.fit.mertijir.pong.controller.IPongCommunication;
import cz.cvut.fit.mertijir.pong.utility.Serializer;

/**
 * Created by Jiří Mertin on 23.1.17.
 */

public class DataRequest implements AJServiceSupportedSerializable {
    public final DataType mDataType;

    public DataRequest(DataType gameStats) {
        mDataType = gameStats;
    }

    @Override
    public void sendThrough(@NonNull IPongCommunication pongCommunicator) {
        try {
            pongCommunicator.sendDataRequest(Serializer.serialize(this));
        } catch (BusException e) {
            e.printStackTrace();
        }
    }
}
