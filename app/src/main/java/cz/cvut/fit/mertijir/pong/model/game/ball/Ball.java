package cz.cvut.fit.mertijir.pong.model.game.ball;

import android.graphics.Canvas;

import cz.cvut.fit.mertijir.pong.App;

/**
 * Created by Jiri Mertin on 11.12.2015.
 */
public abstract class Ball {
    protected int mSize;
    protected int mX;
    protected int mY;

    public Ball() {
        mSize = App.getLocalProportions().getBallSize();
    }

    public abstract void update();

    public abstract void setRunning(final boolean running);

    public abstract void draw(Canvas canvas);

    protected int getSize() {
        return mSize;
    }

    public int getMiddleX() {
        return mX + getSize() / 2;
    }

    public int getMiddleY() {
        return mY + getSize() / 2;
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }
}
