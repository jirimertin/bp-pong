package cz.cvut.fit.mertijir.pong.view.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceFragmentCompat;

import cz.cvut.fit.mertijir.pong.R;
import cz.cvut.fit.mertijir.pong.view.OneFragmentActivity;

/**
 * Created by Jiří Mertin on 30.12.16.
 */

public class GamePreferencesActivity extends OneFragmentActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(new GamePreferencesFragment());
    }

    public static class GamePreferencesFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            addPreferencesFromResource(R.xml.game_preferences);
        }
    }
}
