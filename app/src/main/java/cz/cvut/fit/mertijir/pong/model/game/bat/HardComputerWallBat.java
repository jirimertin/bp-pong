package cz.cvut.fit.mertijir.pong.model.game.bat;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;

/**
 * Created by Jiří Mertin on 15.10.16.
 */
public class HardComputerWallBat extends ComputerWallBat {
    public HardComputerWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
    }

    @Override
    protected void endingMovement(LocalBall ball) {
    }

    @Override
    public PongPreferences.BatControlType getControlType() {
        return PongPreferences.BatControlType.CPU_HARD;
    }
}
