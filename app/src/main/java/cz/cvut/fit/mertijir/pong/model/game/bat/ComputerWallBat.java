package cz.cvut.fit.mertijir.pong.model.game.bat;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.communication.client.PlayersEvent;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.ball.LocalBall;

/**
 * Created by Jiri Mertin on 26.12.2015.
 */
public abstract class ComputerWallBat extends WallBat {

    public ComputerWallBat(LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        super(position, batPosition);
    }

    @Override
    public void move(LocalEventValues eventValues) {
        moveByBalls(eventValues.getBalls());
        super.move(eventValues);
    }

    @Override
    public void moveRemote(PlayersEvent playersEvent) {
        move(null);
    }

    private void moveByBalls(ArrayList<LocalBall> balls) {
        final int maximalSpeed = getMaximalSpeed();
        ArrayList<LocalBall> dangerousBalls = new ArrayList<>(2);
        for (LocalBall b : balls) {
            if (b.getCollisionWallBat() == this) {
                dangerousBalls.add(b);
            }
        }
        if (dangerousBalls.size() == 0) return;
        LocalBall ball = dangerousBalls.get(0);
        for (int i = 1; i < dangerousBalls.size(); i++) {
            ball = closerBall(ball, dangerousBalls.get(i));
        }
        if (ball == null) return;
        int ballSpeed = ball.getSpeed();
        switch (getPosition()) {
            case LEFT:
            case RIGHT:
                int absSpeedY = ball.getAbsSpeedY();
                if (ball.getMiddleY() > mY + getSizeY()) {
                    if (ballSpeed == 0) mY += maximalSpeed;
                    mY += (maximalSpeed > ballSpeed) ? (ballSpeed + absSpeedY) >> 1 : maximalSpeed;
                    break;
                }
                if (ball.getMiddleY() < mY) {
                    if (ballSpeed == 0) mY -= maximalSpeed;
                    mY -= (maximalSpeed > ballSpeed) ? (ballSpeed + absSpeedY) >> 1 : maximalSpeed;
                    break;
                }
                endingMovement(ball);
                break;
            case TOP:
            case BOTTOM:
                int absSpeedX = ball.getAbsSpeedX();
                if (ball.getMiddleX() > mX + getSizeX()) {
                    if (ballSpeed == 0) mX += maximalSpeed;
                    mX += (maximalSpeed > ballSpeed) ? (ballSpeed + absSpeedX) >> 1 : maximalSpeed;
                    break;
                }
                if (ball.getMiddleX() < mX) {
                    if (ballSpeed == 0) mX -= maximalSpeed;
                    mX -= (maximalSpeed > ballSpeed) ? (ballSpeed + absSpeedX) >> 1 : maximalSpeed;
                    break;
                }
                endingMovement(ball);
                break;
        }
    }

    private LocalBall closerBall(LocalBall b1, LocalBall b2) {
        switch (getPosition()) {
            case TOP:
                return (b1.getMiddleY() < b2.getMiddleY()) ? b1 : b2;
            case BOTTOM:
                return (b1.getMiddleY() < b2.getMiddleY()) ? b2 : b1;
            case LEFT:
                return (b1.getMiddleX() < b2.getMiddleX()) ? b1 : b2;
            case RIGHT:
                return (b1.getMiddleX() < b2.getMiddleX()) ? b2 : b1;
        }
        return null;
    }

    protected abstract void endingMovement(LocalBall ball);
}
