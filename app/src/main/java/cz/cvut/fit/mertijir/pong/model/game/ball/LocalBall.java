package cz.cvut.fit.mertijir.pong.model.game.ball;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cz.cvut.fit.mertijir.pong.App;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.bat.Bat;
import cz.cvut.fit.mertijir.pong.model.game.bat.WallBat;

/**
 * Created by Jiří Mertin on 10.1.17.
 */

public class LocalBall extends Ball {
    private int mSpeed;
    private int mSpeedX;
    private int mSpeedY;

    private final int SPEED_INCREMENT;
    private final int SPEED_MAXIMAL;
    private static final long BALL_DELAY = 1000;

    private final ArrayList<Rect> mLastCollisions = new ArrayList<>();
    private boolean mRunning;
    private WallBat mCollisionWallBat;
    private final LocalProportions.WallBatDecision mWallBatDecision;
    private final boolean mPrimaryBall;

    public LocalBall(LocalProportions.WallBatDecision wallBatDecision) {
        mX = (App.getLocalProportions().getWidth() - getSize()) >> 1;
        mY = (App.getLocalProportions().getHeight() - getSize()) >> 1;

        Random random = App.getRandom();
        SPEED_INCREMENT = App.getLocalProportions().getBallSpeedIncrement();
        SPEED_MAXIMAL = App.getLocalProportions().getSpeedMaximal();
        mSpeed = App.getLocalProportions().getBallSpeedStart();
        boolean directionX, directionY;
        directionX = random.nextBoolean();
        directionY = random.nextBoolean();
        setSpeedByAngle(getAngle(
                new Point((directionX) ? 1 : 0, (directionY) ? 1 : 0),
                new Point((directionX) ? 0 : 1, (directionY) ? 0 : 1)));
        mWallBatDecision = wallBatDecision;
        mCollisionWallBat = mWallBatDecision.getFirstBat(this);
        mPrimaryBall = true;
    }

    public LocalBall(LocalBall oppositeTo) {
        mSize = oppositeTo.getSize();
        mX = oppositeTo.mX;
        mY = oppositeTo.mY;
        SPEED_INCREMENT = oppositeTo.SPEED_INCREMENT;
        SPEED_MAXIMAL = oppositeTo.SPEED_MAXIMAL;
        mSpeed = oppositeTo.mSpeed;
        mSpeedX = -oppositeTo.mSpeedX;
        mSpeedY = -oppositeTo.mSpeedY;
        mWallBatDecision = oppositeTo.mWallBatDecision;
        mCollisionWallBat = mWallBatDecision.getFirstBat(this);
        mPrimaryBall = false;
    }

    @Override
    public void update() {
        if (mRunning) {
            mX += mSpeedX;
            mY += mSpeedY;
        }
    }

    public boolean collide(Rect rect) {
        if (!rect.intersects(mX, mY, mX + getSize(), mY + getSize())) {
            mLastCollisions.remove(rect);
            return false;
        }
        if (mLastCollisions.contains(rect)) {
            return true;
        }
        mLastCollisions.add(rect);
        boolean leftTopCorner = rect.contains(mX, mY);
        boolean rightTopCorner = rect.contains(mX + getSize(), mY);
        boolean leftBottomCorner = rect.contains(mX, mY + getSize());
        boolean rightBottomCorner = rect.contains(mX + getSize(), mY + getSize());

        Rect ball = new Rect(mX, mY, mX + getSize(), mY + getSize());
        Rect intersection = new Rect(rect);
        if (!intersection.intersect(ball)) {
            mLastCollisions.remove(rect);
            return false;
        }
        if (intersection.equals(ball)) {
            rotate(Direction.HORIZONTAL);
            rotate(Direction.VERTICAL);
            return true;
        }

        if (intersection.centerX() == ball.centerX()) {
            if (leftTopCorner) {
                makeDirection(LocalProportions.Position.BOTTOM);
            } else {
                makeDirection(LocalProportions.Position.TOP);
            }
            return true;
        }
        if (intersection.centerY() == ball.centerY()) {
            if (leftTopCorner) {
                makeDirection(LocalProportions.Position.RIGHT);
            } else {
                makeDirection(LocalProportions.Position.LEFT);
            }
            return true;
        }

        if ((leftTopCorner && getDirection(Direction.HORIZONTAL) == LocalProportions.Position.LEFT && getDirection(Direction.VERTICAL) == LocalProportions.Position.TOP) ||
                (rightTopCorner && getDirection(Direction.HORIZONTAL) == LocalProportions.Position.RIGHT && getDirection(Direction.VERTICAL) == LocalProportions.Position.TOP) ||
                (leftBottomCorner && getDirection(Direction.HORIZONTAL) == LocalProportions.Position.LEFT && getDirection(Direction.VERTICAL) == LocalProportions.Position.BOTTOM) ||
                (rightBottomCorner && getDirection(Direction.HORIZONTAL) == LocalProportions.Position.RIGHT && getDirection(Direction.VERTICAL) == LocalProportions.Position.BOTTOM)) {
            Random random = App.getRandom();
            switch (random.nextInt(3)) {
                case 0:
                    rotate(Direction.HORIZONTAL);
                    break;
                case 1:
                    rotate(Direction.VERTICAL);
                    break;
                case 2:
                    rotate(Direction.HORIZONTAL);
                    rotate(Direction.VERTICAL);
            }
            return true;
        }

        if (leftTopCorner) {
            makeDirection(LocalProportions.Position.BOTTOM);
            makeDirection(LocalProportions.Position.RIGHT);
        }
        if (rightTopCorner) {
            makeDirection(LocalProportions.Position.BOTTOM);
            makeDirection(LocalProportions.Position.LEFT);
        }
        if (leftBottomCorner) {
            makeDirection(LocalProportions.Position.RIGHT);
            makeDirection(LocalProportions.Position.TOP);
        }
        if (rightBottomCorner) {
            makeDirection(LocalProportions.Position.TOP);
            makeDirection(LocalProportions.Position.LEFT);
        }
        return true;
    }

    private boolean collide(Bat bat) {
        if (!bat.getDrawableRect().intersects(mX, mY, mX + getSize(), mY + getSize())) {
            return false;
        }
        if (bat instanceof WallBat) {
            WallBat wallBat = (WallBat) bat;
            switch (wallBat.getPosition()) {
                case LEFT:
                    if (mX < bat.getX()) return false;
                    break;
                case RIGHT:
                    if (mX + getSize() > bat.getX() + bat.getSizeX()) return false;
                    break;
                case TOP:
                    if (mY < bat.getY()) return false;
                    break;
                case BOTTOM:
                    if (mY + getSize() > bat.getY() + bat.getSizeY()) return false;
                    break;
            }

            int batMiddle;
            int ballMiddle;
            switch (wallBat.getPosition()) {
                case LEFT:
                case RIGHT:
                    batMiddle = bat.getY() + bat.getSizeY() / 2;
                    ballMiddle = mY + getSize() / 2;
                    break;
                default:
                    batMiddle = bat.getX() + bat.getSizeX() / 2;
                    ballMiddle = mX + getSize() / 2;
            }
            double angle;
            switch (wallBat.getPosition()) {
                case LEFT:
                    angle = getAngle(new Point(0, batMiddle), new Point(bat.getSizeX(), ballMiddle));
                    break;
                case RIGHT:
                    angle = getAngle(new Point(bat.getSizeX(), batMiddle), new Point(0, ballMiddle));
                    break;
                case TOP:
                    angle = getAngle(new Point(batMiddle, 0), new Point(ballMiddle, bat.getSizeY()));
                    break;
                default:
                    angle = getAngle(new Point(batMiddle, bat.getSizeY()), new Point(ballMiddle, 0));
            }
            increaseSpeed();
            setSpeedByAngle(angle);
            return true;
        }
        return false;
    }

    public LocalProportions.Position checkBounds(int maxWidth, int maxHeight) {
        if (mX < 0) {
            makeDirection(LocalProportions.Position.RIGHT);
            return LocalProportions.Position.LEFT;
        }
        if (mX + getSize() > maxWidth) {
            makeDirection(LocalProportions.Position.LEFT);
            return LocalProportions.Position.RIGHT;
        }
        if (mY < 0) {
            makeDirection(LocalProportions.Position.BOTTOM);
            return LocalProportions.Position.TOP;
        }
        if (mY + getSize() > maxHeight) {
            makeDirection(LocalProportions.Position.TOP);
            return LocalProportions.Position.BOTTOM;
        }
        return null;
    }

    public static double getAngle(Point from, Point to) {
        double angle = Math.atan2(to.y - from.y, to.x - from.x);
        angle += Math.PI / 2;
        return angle;
    }

    private void setSpeedByAngle(double angle) {
        mSpeedX = (int) (mSpeed * Math.sin(angle));
        mSpeedY = (int) -(mSpeed * Math.cos(angle));
        //Log.d("Speeeeed", "speed " + mSpeed + " X " + mSpeedX + " mY " + mSpeedY);
    }

    private void increaseSpeed() {
        mSpeed += SPEED_INCREMENT;
        if (mSpeed > SPEED_MAXIMAL) {
            mSpeed = SPEED_MAXIMAL;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(mCollisionWallBat.getColor());
        canvas.drawRect(mX, mY, mX + getSize(), mY + getSize(), paint);
        //canvas.drawRect(mCollisionWallBat.getDrawableRect(), paint);
    }


    private void rotate(Direction direction) {
        if (direction == null) return;
        if (direction == Direction.HORIZONTAL) {
            mSpeedX = -mSpeedX;
            return;
        }
        mSpeedY = -mSpeedY;
    }


    private void makeDirection(LocalProportions.Position direction) {
        switch (direction) {
            case LEFT:
            case RIGHT:
                if (getDirection(Direction.HORIZONTAL) != direction) {
                    rotate(Direction.HORIZONTAL);
                }
                break;
            case TOP:
            case BOTTOM:
                if (getDirection(Direction.VERTICAL) != direction) {
                    rotate(Direction.VERTICAL);
                }
        }
    }

    private LocalProportions.Position getDirection(Direction direction) {
        if (direction == null) return null;
        if (direction == Direction.HORIZONTAL) {
            return (mSpeedX > 0) ? LocalProportions.Position.RIGHT : LocalProportions.Position.LEFT;
        }
        return (mSpeedY > 0) ? LocalProportions.Position.BOTTOM : LocalProportions.Position.TOP;
    }

    public int getSpeed() {
        return mSpeed;
    }

    public int getAbsSpeedX() {
        return Math.abs(mSpeedX);
    }

    public int getAbsSpeedY() {
        return Math.abs(mSpeedY);
    }

    public int getSpeedX() {
        return mSpeedX;
    }

    public int getSpeedY() {
        return mSpeedY;
    }

    @Override
    public void setRunning(final boolean running) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                mRunning = running;
            }
        }, running ? BALL_DELAY : 0);
    }

    public void setCollisionWallBat(WallBat collisionWallBat) {
        mCollisionWallBat = collisionWallBat;
    }

    public WallBat getCollisionWallBat() {
        return mCollisionWallBat;
    }

    public void checkWallBatCollision() {
        if (collide(mCollisionWallBat)) {
            setCollisionWallBat(mWallBatDecision.getNextBatFromLastPosition(mCollisionWallBat.getPosition()));
        }
    }

    public boolean isPrimaryBall() {
        return mPrimaryBall;
    }

    private enum Direction {
        HORIZONTAL, VERTICAL
    }
}
