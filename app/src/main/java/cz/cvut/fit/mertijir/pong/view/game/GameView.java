package cz.cvut.fit.mertijir.pong.view.game;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

import cz.cvut.fit.mertijir.pong.controller.PongPreferences;
import cz.cvut.fit.mertijir.pong.model.game.GameLoop;
import cz.cvut.fit.mertijir.pong.model.game.LocalEventValues;
import cz.cvut.fit.mertijir.pong.model.game.LocalProportions;
import cz.cvut.fit.mertijir.pong.model.game.ball.Ball;
import cz.cvut.fit.mertijir.pong.model.game.bat.Bat;
import cz.cvut.fit.mertijir.pong.model.game.bat.WallBat;

/**
 * Created by Jiri Mertin on 8.12.2015.
 */
public abstract class GameView extends SurfaceView implements SensorEventListener {
    private static final String TAG = GameView.class.getName();
    protected GameLoop mGameLoop;
    private boolean mEmpty = true;
    private boolean mPrepareForRun;

    private static float sWalls[];
    private static float sTouchZones[];

    protected LocalEventValues mEventValues;

    protected LocalProportions.WallBatDecision mWallBatDecision;

    public GameView(final Context context) {
        super(context);
        mGameLoop = new GameLoop(this);

        SurfaceHolder surfaceHolder = getHolder();
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (mEmpty) {
                    Log.d(TAG, "width " + getWidth() + " height " + getHeight());

                    sWalls = new float[]{
                            0, 0, getWidth() - 1, 0,
                            0, 0, 0, getHeight() - 1,
                            getWidth() - 1, 0, getWidth() - 1, getHeight() - 1,
                            0, getHeight() - 1, getWidth() - 1, getHeight() - 1
                    };
                    if (getHeight() > getWidth()) {
                        sTouchZones = new float[]{
                                0, getHeight() * 2 / 5, getWidth(), getHeight() * 2 / 5,
                                0, getHeight() * 3 / 5, getWidth(), getHeight() * 3 / 5};
                    } else {
                        sTouchZones = new float[]{
                                getWidth() * 2 / 5, 0, getWidth() * 2 / 5, getHeight(),
                                getWidth() * 3 / 5, 0, getWidth() * 3 / 5, getHeight()
                        };
                    }

                    mEventValues = new LocalEventValues();

                    createGameObjects();
                    Canvas c = null;
                    try {
                        update();
                        c = getHolder().lockCanvas();
                        synchronized (getHolder()) {
                            draw(c);
                        }
                    } finally {
                        if (c != null) {
                            getHolder().unlockCanvasAndPost(c);
                        }
                    }

                    mEmpty = false;
                    setRunningLoop(mPrepareForRun);
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }
        });
    }

    protected abstract void createGameObjects();

    protected WallBat createLocalBat(PongPreferences.BatControlType type, LocalProportions.Position position, PongPreferences.BatPosition batPosition) {
        return WallBat.generateBat(position, type, batPosition);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        // Background
        canvas.drawColor(Color.BLACK);

        // TouchZones
        Paint dashedGray = new Paint();
        dashedGray.setColor(Color.GRAY);
        dashedGray.setStyle(Paint.Style.STROKE);
        dashedGray.setPathEffect(new DashPathEffect(new float[]{10, 20}, 0));
        canvas.drawLines(sTouchZones, dashedGray);

        // Walls
        Paint white = new Paint();
        white.setColor(Color.WHITE);
        canvas.drawLines(sWalls, white);

        // Bats

        for (Bat bat : getBats()) {
            bat.draw(canvas);
        }
        Rect intersection = new Rect();
        for (int i = 0; i < getBats().size(); i++) {
            for (int j = i + 1; j < getBats().size(); j++) {
                if (intersection.setIntersect(getBats().get(i).getDrawableRect(), getBats().get(j).getDrawableRect())) {
                    Paint color = new Paint();
                    color.setColor(getBats().get(i).getColor() + getBats().get(j).getColor());
                    canvas.drawRect(intersection, color);
                }
            }
        }
        for (Ball ball : getBalls()) {
            ball.draw(canvas);
        }
    }

    protected abstract ArrayList<? extends Ball> getBalls();

    public abstract void update();

    public void setRunningLoop(boolean runningLoop) {
        if (mEmpty) {
            mPrepareForRun = runningLoop;
            return;
        }
        mGameLoop.setRunning(runningLoop);
        if (runningLoop && !mGameLoop.isAlive()) {
            mGameLoop = new GameLoop(this);
            mGameLoop.setRunning(true);
            mGameLoop.start();
        }
        for (Ball ball : getBalls()) {
            ball.setRunning(runningLoop);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (!mEmpty) mEventValues.saveSensorEvent((Activity) getContext(), event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mEventValues.saveMotionEvent(event);
        return super.onTouchEvent(event);
    }

    public abstract ArrayList<? extends Bat> getBats();
}
